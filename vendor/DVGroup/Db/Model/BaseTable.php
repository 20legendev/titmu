<?php

namespace DVGroup\Db\Model;

use DVGroup\Redis\Redis;
use DVGroup\Common\CommonLibs;
use Zend\Db\TableGateway\TableGateway;
use DVGroup\Db\Sync\SyncQueue;

class BaseTable {

    protected $tableGateway;
    protected $redis;
    protected $neo4j;

    public function __construct(TableGateway $tableGateway = null) {
        if ($tableGateway) {
            $this->tableGateway = $tableGateway;
        }
        $this->initial();
    }

    public function initial() {
        
    }

    public function getRedis() {
        if (!$this->redis) {
            $this->redis = new Redis();
        }
        return $this->redis;
    }

    public function getLastInsertId() {
        return $this->tableGateway->lastInsertValue;
    }

    public function exchangeArray($data) {
        foreach ($data as $key => $value) {
            $this->$key = (isset($data[$key])) ? $value : null;
        }
    }

    public function getCacheableData($query, $cache_time = 'SHORT') {
        $sql = new \Zend\Db\Sql\Sql($this->tableGateway->getAdapter());
        $query_string = $sql->getSqlStringForSqlObject($query);
        $key = '_CACHE:AUTO_CACHE:' . md5($query_string);
        if ($this->getRedis()->exists($key)) {
            return $this->_get($key);
        }
        $statement = $this->tableGateway->getAdapter()->query($query_string);
        $result = $statement->execute();

        $result_arr = array();
        foreach ($result as $value) {
            $result_arr[] = $value;
        }
        if ($cache_time != 'FOREVER') {
            $config = $this->getConfig();
            $cache_time = $config['CACHE_TIME'][$cache_time];
            $this->_set($key, $result_arr, $cache_time);
        } else {
            $this->_set($key, $result_arr);
        }
        $this->_set($key, $result_arr, $cache_time);
        return $result_arr;
    }

    public function _get($key) {
        $redis = $this->getRedis();
        $data = $redis->get($key);
        if (!$data) {
            return NULL;
        }
        return CommonLibs::UnZip($data);
    }

    public function _set($key, $value, $expire = 7200) {
        $redis = $this->getRedis();
        if ($expire > 0) {
            return $redis->set($key, CommonLibs::Zip($value), $expire);
        } else {
            return $redis->set($key, CommonLibs::Zip($value));
        }
    }
    
    public function addQueue($query) {
        $queue = new SyncQueue($this->tableGateway->getAdapter());
        $queue->add($query);
    }

    protected function getConfig() {
        return include __DIR__ . '/../../../../config/autoload/global.php';
    }

}
