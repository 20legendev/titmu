<?php

namespace DVGroup\Operation;

use Zend\Mvc\Controller\AbstractActionController;
use DVGroup\Redis\Redis;
use Zend\View\Model\ViewModel;

class BaseController extends AbstractActionController {

    protected $redis;
    protected $user;

    public function __construct() {
        $this->tables = array();
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e) {
        return parent::onDispatch($e);
    }

    protected function getTable($table_name) {
        if (isset($this->tables[$table_name])) {
            return $this->tables[$table_name];
        }
        $this->tables[$table_name] = $this->getServiceLocator()->get($table_name);
        return $this->tables[$table_name];
    }

    protected function getRedis() {
        if ($this->redis) {
            return $this->redis;
        }
        $this->redis = new Redis ();
        return $this->redis;
    }

    protected function setResponse($data) {
        $this->getResponse()->getHeaders()->addHeaders(array('Content-Type' => 'application/json;charset=UTF-8'));
        $this->response->setContent(json_encode($data));
    }

    protected function isLoggedIn() {
        $auth = new \DVGroup\Users\AuthUser();
        return $auth->isLoggedIn();
    }
    
    protected function getUser(){
        $auth = new \DVGroup\Users\AuthUser();
        return $auth->getUser();
    }

    protected function setTitle($title = '') {
        $sm = $this->getServiceLocator();
        $config = $sm->get('config');
        $headTitleHelper = $sm->get('viewHelperManager')->get('headTitle');
        $headTitleHelper->set($config['seo']['sitename']);
        $headTitleHelper->setSeparator(' - ');
        $headTitleHelper->append($title);
    }

}

?>