<?php
    /**
    * Author: Phí Đình Thọ
    * Class RedisActive
    * Sử dụng Redis như là database
    * Tạo ra các key là dạng hash ( chứa trong thư mục detail) để lưu dữ liệu chung và đầy đủ, Không dùng các hash này trong các câu querry ,sắp xếp hay các tác vụ khác mà chỉ dùng để lấy ra dữ liệu cuối cùng để hiển thị khi đã tính toán ra các key ID phù hợp, ko cần duyệt thư mục các key hash này nên quá trình ko bị chậm
    * Để sắp xếp, tạo ra các key dạng Sorted Set ( Chứa trong thư mục sort) để phục vụ sort đối với từng thuộc tính
    * Để querry, filter : Tạo ra các key Index ( Chứa trong thư mục Index ) để phục vụ mục đích filter theo điều kiện
    * Để cache: khi gọi ->cached(), thì kết quả câu querry tự động được cache vào hệ thống trong 1 key ở thư mục cache để lần sau chỉ cần gọi key đó là có dũ liệu lấy ra, mỗi khi có bất kì sự thay đổi nào trong database (các sự kiện insert, save, update, updateAll, delete, deleteAll) thì các key này tự động bị xóa để build lại
    * Để validate: Có thể tùy biến thêm các validate phục vụ đảm bảo dữ liệu chính xác (trong khi redis thì ko hỗ trợ và ko cần validate)
    * Hỗ trợ auto value, autoincrement khi insert mới.
    * Các hàm tương tác như db: save, insert,update,delete, updateAll, deleteAll, findAll, findOne, find(),....
    * Hỗ trợ các select tùy biến các thuộc tính, với các hàm count, first, last, distinct, sum, average, ....
    * Gọi theo phương thức ORM vd:$findModel = TbChat::find()->select(['user_from','content','date'])->where(['room_id'=>5,['like','content','%c'])->orWhere(['>=','id',5])->limit(20)->orderBy(['content'=>SORT_DESC])->asArray()->cached()->all(); 
    * Thư viện này độc lập với framework, nó đi kèm với thư viện Predis của Redis và có thể dễ dàng mang sang sử dụng ở các frame work khác, chỉ cần viết lại đoạn "use Zend\Predis\Client;" và hàm _construct để sửa đường dẫn đến file config. Về Predis,  tham khảo: http://www.sitepoint.com/an-introduction-to-redis-in-php-using-predis/
    * Và các tính năng khác dễ dàng phát triển tiếp  (Một số tính năng quan trọng chưa phát triển: with(), hasOne, hasMany, groupBy)
    */
    namespace DVGroup\Redis;
    use Zend\Predis\Client; 
    class RedisActive extends Client  implements \ArrayAccess
    { 
        private $_attributes = [];
        public $errors = [];
        public $attributes = [];
        private $_select = null; 
        private $_condition = [];
        private $_orCondition = [];
        private $_limit = null;
        private $_orderBy = null;
        private $_asArray = false;
        private $_cached = false; 

        /**
        *  Khai báo và trả về các thuộc tính của model, phải overwrite lại ở class con
        * Eg: return ['id','user_from','user_to','room_id','content','date'];
        */
        public function attributes()
        {
            throw new \Exception('Attributes must be overwriten in the child Class');
            return false;
        }

        /**
        *  Validate dữ liệu, lưu ý, để đảm bảo model chạy chính xác thì tất cả thuộc tính đều phải validate
        *  Các validate đã viết sẵn : bao gồm hàm (required, unique, integer, float, autoIncrement, datetime,...) và method (min, max, >,>=,<,<=,like, notlike...) , Có thể phát triển thêm bất kì validate nào nếu muốn 
        *  Chú ý: Hàm này phải viết ở Class con
        *  unique thì ví dụ như sau [[['attr1','attr2'],['attr3']],'unique','messages'=>['attr1'=>'attr1 , attr2 must be unique','attr2'=>'attr1 , attr2 must be unique','attr3' => 'attr3 must be unique']];
        *  Eg: return [
        [['user_from','content'], 'required','messages'=>['user_from'=>'user_from cannot be blank','content' => 'content cannot be blank']],
        [['id','room_id'], 'integer','messages'=>['id'=>'id must be an integer','room_id'=>'room_id must be an integer']],
        [['id'], 'autoIncrement'],
        [['date'], 'datetime','messages'=>['date'=>'datetime value is invalid']],
        [['content'], ['max',255],'messages'=>['content'=>'content must have less than 255 chars']],
        ];
        */
        public function rules()
        {
            throw new \Exception('Rules must be overwriten in the child Class');
            return false;
        }
        /**
        *  Khai báo và trả về khóa chính của model (Mặc định là id, Có thể overwrite ở class con)
        */
        public static function primaryKey()
        {
            $className = get_called_class();
            $model = new $className();
            if ($model->hasAttribute('id'))
                return 'id';
            else throw new \Exception('Model must have a primaryKey and this function primaryKey() must be re-declared in the child Class');
        }

        /**
        *  Khai báo và trả về prefix của các key trong model
        */
        public static function keyPrefix()
        {
            $regex = '/(?<![A-Z])[A-Z]/';
            return trim(strtolower(preg_replace($regex, '_\0', self::className())), '_');
        } 

        /**
        *  Trả về đối tượng Class được gọi
        */
        public static function className()
        {
            $className = get_called_class();
            $className = rtrim(str_replace('\\', '/', $className), '/\\');
            if (($pos = mb_strrpos($className, '/')) !== false) {
                $className = mb_substr($className, $pos + 1);
            }
            return $className;
        }        

        /**
        *  Khởi tạo đối tượng Predis
        *  Config lưu trong APPLICATION_PATH."\\config\\autoload\\global.php"
        *  example: 'redis-sv' => array(
        'scheme'   => 'tcp',
        'database' => 0,
        'port'     => 6388,
        'host'     => '172.16.0.13',
        ),
        */
        public function __construct($sv = 'sv1') {
            ini_set('memory_limit', '512M');
            if (self::className() != 'RedisActive') $this->attributes = $this->attributes();
            $config = require APPLICATION_PATH."/config/autoload/global.php"; 
            if (isset($config['redis-sv'])) parent::__construct($config['redis-sv'][$sv]);
            else return null;
        }

        /**
        *  Nạp các thuộc tính vào model
        *  @data Array | Mảng key => value (thuộc tính => giá trị)
        */
        public function load($data){
            foreach ($this->attributes() as $attribute) {
                $this->$attribute = isset($data[$attribute]) ? $data[$attribute]:'';
            }
        }

        /**
        *  Các Hàm viết để Implement vào ArrayAccess (class có sẵn của php) để có thể sử dụng Class như là Array
        *  Có 4 hàm là offsetExists, offsetGet, offsetSet, offsetUnset
        */
        public function offsetExists ($offset)
        {
            return $this->hasAttribute($offset);
        }

        /**
        *  Các Hàm viết để Implement vào ArrayAccess (class có sẵn của php) để có thể sử dụng Class như là Array
        *  Có 4 hàm là offsetExists, offsetGet, offsetSet, offsetUnset
        */
        public function offsetGet ($offset)
        {
            return $this->$offset;
        }

        /**
        *  Các Hàm viết để Implement vào ArrayAccess (class có sẵn của php) để có thể sử dụng Class như là Array
        *  Có 4 hàm là offsetExists, offsetGet, offsetSet, offsetUnset
        */
        public function offsetSet ($offset, $value)
        {
            $this->$offset = $value;
        }

        /**
        *  Các Hàm viết để Implement vào ArrayAccess (class có sẵn của php) để có thể sử dụng Class như là Array
        *  Có 4 hàm là offsetExists, offsetGet, offsetSet, offsetUnset
        */
        public function offsetUnset ($offset)
        {
            $this->$offset = null;
        }

        /**
        *  Kiểm tra xem model có thuộc tính này hay không
        *  @name string | Tên thuộc tính
        */
        public function hasAttribute($name)
        {
            return isset($this->_attributes[$name]) || in_array($name, $this->attributes());
        }

        /**
        *  Truyền giá trị cho 1 thuộc tính của model
        *  @name string | Tên thuộc tính
        *  @value string | Giá trị của thuộc tính
        */
        public function __set($name, $value)
        {
            if ($this->hasAttribute($name)) {
                $this->_attributes[$name] = $value;
                if (self::getTypeAttribute($name) == 'integer') $this->_attributes[$name] = (int)$value;
            } 
        }

        /**
        *  Lấy giá trị cao nhất của thuộc tính thuộc kiểu integer, nếu không tồn tại thì tạo mới với giá trị =0;
        *  @name string | Tên thuộc tính
        */
        public function getCurentKey($name)
        {
            $currentKey = 0;
            if ($this->hasAttribute($name)) {
                $key = self::keyPrefix().':last:'.$name;                
                $currentKey = $this->get($key);
                if (!$currentKey){
                    $currentKey = 0;
                    $this->set($key,0);
                }
            }
            return $currentKey; 
        }

        /**
        *  Đặt giá trị cao nhất cho thuộc tính thuộc kiểu integer, nếu không tồn tại thì tạo mới với giá trị =0;
        *  @name string | Tên thuộc tính
        */
        public function setCurentKey($name,$value)
        {  
            if ($this->hasAttribute($name)) {
                $key = self::keyPrefix().':last:'.$name;
                $this->set($key,$value);
            } 
        }

        /**
        *  Lấy ra giá trị cho 1 thuộc tính của model
        *  @name string | Tên thuộc tính
        */
        public function __get($name)
        {
            if (isset($this->_attributes[$name]) || array_key_exists($name, $this->_attributes)) {
                return $this->_attributes[$name];
            } elseif ($this->hasAttribute($name)) {
                return null;
            } 
        }

        /**
        *  Kiểm tra model có lỗi hay không
        */
        public function hasErrors(){
            return !$this->validate();
        }

        /**
        *  Validate các thuộc tính của model
        */
        public function validate($isInsert=false){
            $this->errors = [];
            if (count($this->rules()))
                foreach ($this->rules() as $rule) {
                    $attributes = $rule[0];
                    $ruleDetail = $rule[1];
                    $messages = isset($rule['messages']) ? $rule['messages']:[];
                    foreach ($attributes as $attribute) {
                        if (!is_array($ruleDetail)){
                            if(!$this->$ruleDetail($attribute,$isInsert)) {
                                if (!is_array($attribute)) $this->errors[$attribute][]= isset($messages[$attribute]) ?  $messages[$attribute]:'Error on validate '.$attribute;
                                else {
                                    foreach ($attribute as $attr) $this->errors[$attr][]= isset($messages[$attr]) ?  $messages[$attr]:'Error on validate '.$attr;
                                }
                            }
                        }else{
                            $method = $ruleDetail[0];
                            $value = $ruleDetail[1]; 
                            if(!$this->validateByMethod($attribute,$method,$value,$isInsert)) {
                                $this->errors[$attribute][]= isset($messages[$attribute]) ?  $messages[$attribute]:'Error on validate '.$attribute;
                            }
                        }
                    }                
            } 
            return count($this->errors) ? false:true;                                          
        }

        /**
        *  Validate thuộc tính bằng một số đặc tính định nghĩa sẵn (có thể phát triển tiếp và định nghĩa thêm các method)
        *  @attribute string | Tên thuộc tính
        */
        public function validateByMethod($attribute,$method,$value,$isInsert=false){
            switch ($method) {
                case 'max':
                    if (strlen($this->$attribute) <= $value) return true;
                    break;

                case 'min':
                    if (strlen($this->$attribute) >= $value) return true;
                    break;

                case '>':
                    if ($this->$attribute > $value) return true;
                    break;

                case '<':
                    if ($this->$attribute < $value) return true;
                    break;

                case '=':
                    if ($this->$attribute == $value) return true;
                    break;

                case '>=':
                    if ($this->$attribute >= $value) return true;
                    break;

                case '<=':
                    if ($this->$attribute <= $value) return true;
                    break;

                case 'like':
                    if (strpos($this->$attribute,$value) !== false) return true;
                    break;

                case 'notlike':
                    if (strpos($this->$attribute,$value) === false) return true;
                    break;
                default:
                    throw new \Exception('Unsuppoted method: '.$method);
                    break;
            }   
            return false;
        }

        /**
        *  Kiểm tra xem giá trị có rỗng hay không
        *  @attribute string | Tên thuộc tính
        */
        public function required($attribute,$isInsert=false){
            return (($this->$attribute !=='') && ($this->$attribute !==null) && ($this->$attribute !==false));            
        }

        /**
        *  Kiểm tra xem các giá trị có unique không
        *  @attribute string | Tên thuộc tính
        */
        public function unique($attribute,$isInsert=false){ 
            $className = get_called_class();
            $model = new $className();
            foreach ($attribute as $attr) {
                $model->andWhere([$attr=>$this->$attr]); 
            }
            $primaryKey = self::primaryKey();     
            $model->andWhere(['!=',$primaryKey,$this->$primaryKey]);         
            return !$model->one();
        }

        /**
        *  Kiểm tra xem giá trị có phải là số nguyên
        *  @attribute string | Tên thuộc tính
        */
        public function integer($attribute,$isInsert=false){
            return ($this->$attribute !== null) ? is_int($this->$attribute):true;            
        } 

        /**
        *  Kiểm tra xem giá trị có phải là float
        *  @attribute string | Tên thuộc tính
        */
        public function float($attribute,$isInsert=false){
            return ($this->$attribute !== null) ? is_numeric($this->$attribute):true;            
        }   

        /**
        *  Kiểm tra xem nếu thuộc tính thời gian có giá trị rỗng thì gán bằng giá trị thời gian hiện tại
        *  @attribute string | Tên thuộc tính
        */
        public function datetime($attribute,$isInsert=false) {
            if (!$this->$attribute) $this->$attribute = date('Y-m-d H:i:s',time());
            else return strtotime($this->$attribute) ? true:false;
            return true;  
        }

        /**
        *  Tự động tăng giá trị của thuộc tính
        *  @attribute string | Tên thuộc tính
        */
        public function autoIncrement($attribute,$isInsert=false) {
            $currentKey = $this->getCurentKey($attribute);
            if (!$this->$attribute) {
                $currentKey ++ ;
                $this->$attribute = $currentKey;
                if (self::getTypeAttribute($attribute) == 'integer') $this->$attribute = (int)$currentKey;
                if ($isInsert) $this->setCurentKey($attribute,$currentKey);
            }else{
                if ($isInsert){
                    if ($this->$attribute > $currentKey) $this->setCurentKey($attribute,$this->$attribute);
                }
            } 
            return true;
        }

        /**
        *  Thêm một row mới vào bảng (Validate là bắt buộc)
        */
        public function insert()
        {
            if ($this->validate()){
                $this->validate(true);
                $primaryKey = self::primaryKey();
                if (!$this->$primaryKey) $this->autoIncrement($primaryKey);
                foreach ($this->attributes() as $attribute) {
                    if (!isset($this->_attributes[$attribute])) $this->_attributes[$attribute] =''; 
                }  
                $this->hMSet(self::keyPrefix().':detail:'.$this->$primaryKey,$this->_attributes);
                $indexKeys = $this->keys(self::keyPrefix().":index:*");
                if  (!$indexKeys) {
                    $this->hSet(self::keyPrefix().':index:'.$primaryKey,$this->$primaryKey,$this->$primaryKey);    
                }                      
                else {
                    foreach ($indexKeys as $key) {
                        $attribute = substr($key,strlen(self::keyPrefix().':index:'));
                        $this->hSet(self::keyPrefix().':index:'.$attribute,$this->$primaryKey,$this->$attribute);    
                    }   
                }
                $sortKeys = $this->keys(self::keyPrefix().":sort:*");
                if ($sortKeys) {
                    foreach ($sortKeys as $key) {
                        $attribute = substr($key,strlen(self::keyPrefix().':sort:'));
                        $this->zAdd(self::keyPrefix().':sort:'.$attribute,self::getScoreValue($attribute,$this->$attribute),$this->$primaryKey);
                    } 
                }
                self::deleteCache();
                return true;
            }
            else{
                return false;
            }
        }

        /**
        *  Update một row (Validate là bắt buộc)
        */
        public function update()
        {
            $primaryKey = self::primaryKey();
            if ($this->$primaryKey){
                return $this->insert();
            }    
            else
                return false;
        }

        /**
        *  Save một row (Validate là bắt buộc)
        */
        public function save()
        {
            return $this->insert();
        }

        /**
        *  delete một row
        */
        public function delete()
        {
            $primaryKey = self::primaryKey();  
            $indexKeys = $this->keys(self::keyPrefix().":index:*");
            if ($indexKeys){
                foreach ($indexKeys as $key) {
                    $this->hDel($key,$this->$primaryKey);    
                } 
            }
            $sortKeys = $this->keys(self::keyPrefix().":sort:*");
            if ($sortKeys) {
                foreach ($sortKeys as $key) {
                    $this->zRem($key,$this->$primaryKey);
                } 
            }
            $this->del(self::keyPrefix().':detail:'.$this->$primaryKey);
            self::deleteCache();
        }

        /**
        *  Update theo điều kiện
        *  @condition  Array
        *  @attributes  Array (Mảng Thuộc tính => giá trị)
        */
        public static function updateAll($condition,$attributes)
        {
            $findModels = self::findAll($condition);
            if ($findModels){
                foreach ($findModels as $model) {
                    $model->load($attributes);
                    $model->update();
                }
                self::deleteCache();
            }   
        }

        /**
        *  Xóa theo điều kiện
        *  @condition Array
        */
        public static function deleteAll($condition = '')
        {
            if ($condition) {
                $findModels = self::findAll($condition);
                if ($findModels){
                    foreach ($findModels as $model) $model->delete();
                }
                self::deleteCache();   
            }else {
                $className = get_called_class();
                $db = new $className();
                $allKeys = $db->keys(self::keyPrefix()."*");
                if ($allKeys && (count($allKeys))){
                    foreach ($allKeys as $key) {
                        $db->del($key);
                    }
                }  
            }
        }

        /**
        *  Tìm kiếm theo điều kiện
        *  @condition Array
        */
        public static function findAll($condition)
        { 
            $className = get_called_class();
            $model = new $className();
            $model->_condition = $condition;
            return $model->all();   
        }

        /**
        *  Tìm kiếm theo khóa chính
        *  @primaryKey Khóa chính
        *  @asArray bool | Trả về dữ liệu kiểu mảng
        */
        public static function findOne($primaryKey,$asArray = false)
        {
            $className = get_called_class();
            $model = new $className();
            $value = $model->hGetAll(self::keyPrefix().':detail:'.$primaryKey);
            if ($value) {  
                $model->load($value);
                if ($asArray) return $model->_attributes;
                else return $model;  
            }
            return false;
        }

        /**
        *  Trả về một đối tượng tìm kiếm
        *  @condition Array
        */
        public static function find($condition='')
        {
            $className = get_called_class();
            $model = new $className();
            if ($condition) $model->_condition = $condition;
            return $model;
        } 

        /**
        *  Trả về kết quả đầu tiên tìm thấy
        */
        public function one()
        {   
            $data = $this->getData();
            if ($data) return $this->selectData(self::findOne($data[0],$this->_asArray),'one');
            return null;
        }

        /**
        *  Trả về tất cả các kết quả tìm kiếm  
        */
        public function all()
        { 
            $return = null;
            $data = $this->getData();
            if ($data) {
                $return =[];
                foreach ($data as $item) {
                    $return[] = self::findOne($item,$this->_asArray);
                }
            }
            if ($return) return $this->selectData($return,'all');
            else return null;
        }

        /**
        *  Trả về kết quả tìm kiếm theo select 
        *  @data Array | Kết quả sau filter
        *  @selectType string | = 'one' hoặc 'all'
        */ 
        public function selectData($data, $selectType)
        {
            $result = $data;
            if ($this->_select){
                if (!is_array($this->_select)) {
                    switch ($this->_select) {
                        case 'count':
                            $result = count($result);
                            break;
                        case 'first':
                        switch ($selectType) {
                            case 'one':
                                $result = $data;
                                break;
                            case 'all':
                                if ($data) $result = $data[0];
                                break;
                            default:
                                break;
                        }
                        break;
                        case 'last':
                        switch ($selectType) {
                            case 'one':
                                $result = $data;
                                break;
                            case 'all':
                                if ($data) $result = $data[count($data)-1];
                                break;
                            default:
                                break;
                        }
                        break;
                        // Viết thêm các trường hợp select khác tại đây
                        //
                        //
                        default:
                            break;
                    }
                }else{
                    if (count($this->_select) > 0){
                        if (!is_array($this->_select[0])){
                            foreach ($this->_select as $attribute) {
                                if (!$this->hasAttribute($attribute)) 
                                    throw new \Exception($attribute. 'is not exist in this model');
                            }
                            if ($result){
                                switch ($selectType){
                                    case 'one':
                                        foreach ($this->attributes() as $attribute) if (!in_array($attribute,$this->_select)) {
                                            if ($this->_asArray){
                                                unset($result[$attribute]);   
                                            }else{
                                                $result->attributes = $this->_select; 
                                            } 
                                        }
                                        break;
                                    case 'all':
                                        if ($result) {
                                            foreach ($result as $key => $row) {
                                                foreach ($this->attributes() as $attribute) if (!in_array($attribute,$this->_select)) 
                                                {
                                                    if ($this->_asArray){
                                                        unset($result[$key][$attribute]);  
                                                    }else{
                                                        $result[$key]->attributes = $this->_select;   
                                                    }    
                                                }
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } 
                        else
                        {
                            if (!isset($this->_select[1])) throw new \Exception('Invalid select configuration');
                            $method = $this->_select[1];
                            switch ($method) {
                                case 'distinct':
                                switch ($selectType){
                                    case 'one':
                                        break;                                        
                                    case 'all':
                                        $newData = [];
                                        $check = [];
                                        $attributes = $this->_select[0];
                                        if ($result){
                                            foreach ($result as $item) {
                                                $itemcheck =[];
                                                foreach ($attributes as $attr) $itemcheck[$attr] = $item[$attr];
                                                $itemcheck = json_encode($itemcheck);
                                                if (!in_array($itemcheck,$check)) {
                                                    $newData[] = $item;
                                                    $check[] = $itemcheck;
                                                }
                                            }                        
                                        }   
                                        $result = $newData;
                                        break;
                                    default:
                                        break;
                                }
                                break;
                                case 'sum':
                                    $attribute = $this->_select[0][0];
                                    if (!$this->hasAttribute($attribute)) throw new \Exception($attribute. 'is not exist in this model');
                                    else{
                                        $total = 0;
                                        if ($result){ 
                                            switch ($selectType){
                                                case 'one':
                                                    $total = $result[$attribute];
                                                    break;
                                                case 'all':
                                                    foreach ($result as $row) {
                                                        $total += $row[$attribute];
                                                    }
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        $result = $total;
                                    }
                                    break;
                                case 'average':
                                    $attribute = $this->_select[0][0];
                                    if (!$this->hasAttribute($attribute)) throw new \Exception($attribute. 'is not exist in this model');
                                    else{
                                        $total = 0;
                                        $average = 0;
                                        if ($result){ 
                                            switch ($selectType){
                                                case 'one':
                                                    $total = $result[$attribute];
                                                    $average = $total;
                                                    break;
                                                case 'all':
                                                    $count = 0;
                                                    foreach ($result as $row) {
                                                        $total += $row[$attribute];
                                                        $count ++;
                                                    }
                                                    $average = $total / $count;
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                        $result = $average ;
                                    }
                                    break;
                                    // Viết thêm các trường hợp khác tại đây
                                    //
                                    //
                                default:
                                    break;
                            }
                        }                        
                    }                    
                }
            }
            return $result;
        }

        /**
        *  Khai báo các thuộc tính cần lấy và đặc tính (ví dụ distinct)
        *  @select string | Array  
        *  Eg 'count' Lấy số lượng kết quả
        *  Eg 'first' Lấy kết quả đầu tiên
        *  Eg 'last' Lấy kết quả cuối cùng
        *  Eg ['id','room_id','content']
        *  Eg [['id','room_id','content'],'distinct']
        *  Eg ['room_id','sum']    Tính tổng
        *  Eg ['room_id','average']  Lấy trung bình
        *  Và có thể phát triển thêm nhiều hàm khác để tiện việc sử dụng
        */
        public function select($select)
        { 
            $this->_select = $select;
            return $this;
        }

        /**
        *  Đặt điều kiện tìm kiếm 
        *  @condition Array 
        */
        public function where($condition)
        { 
            $this->_condition = $condition;
            return $this;
        }

        /**
        *  Thêm vào điều kiện tìm kiếm
        *  @condition Array 
        */
        public function andWhere($condition)
        { 
            $this->_condition = array_merge($this->_condition,$condition);
            return $this;
        }

        /**
        *  Thêm vào điều kiện tìm kiếm dạng OR
        *  @condition Array 
        */
        public function orWhere($condition)
        { 
            $this->_orCondition = array_merge($this->_orCondition,$condition);
            return $this;
        }

        /**
        *  Trả về dữ liệu theo giới hạn
        *  @limit số lượng hàng tối đa
        *  @offet Bắt đầu từ vị trí (Đối với primaryKey)
        *  @greater bool | lấy lớn hơn hay nhở hơn 
        */        
        public function limit($limit,$offset = null,$greater = true)
        { 
            if (!$offset) $offset = 0;
            $this->_limit = ['limit' => $limit, 'offset' => $offset, 'greater' => $greater];
            return $this;
        }

        /**
        *  Sắp xếp dữ liệu trả về
        *  @orderBy Array
        */
        public function orderBy($orderBy)
        { 
            $this->_orderBy = $orderBy;
            return $this;
        }

        /**
        *  Chỉ lấy dữ liệu dạng Array 
        */
        public function asArray()
        { 
            $this->_asArray = true;
            return $this;
        }

        /**
        *  Cache kết quả của câu querry (Cache này sẽ bị xóa khi có sự kiện insert, update, delete)
        */
        public function cached()
        { 
            $this->_cached = true;
            return $this;
        }

        /**
        *  Xóa các key cache
        */
        public static function deleteCache()
        { 
            $className = get_called_class();
            $db = new $className();
            $allKeys = $db->keys(self::keyPrefix().":cached:*");
            if ($allKeys && (count($allKeys))){
                foreach ($allKeys as $key) {
                    $db->del($key);
                }
            }          
        }

        /**
        *  Trả về mảng primary key sau khi sắp xếp theo $attribute với kiểu sắp xếp $sort
        *  @attribute string
        */
        public function getSort($attribute,$sort = SORT_ASC){
            $result = null;
            $method = ($sort == SORT_ASC) ? 'zRange':'zRevRange';
            $key = self::keyPrefix().':sort:'.$attribute;
            $result = $this->$method($key,0,-1);
            if (!$result){              
                $allPrimary = $this->hGetAll(self::keyPrefix().':index:'.self::primaryKey());
                if ($allPrimary){
                    foreach ($allPrimary as $item) {
                        $modelKey = self::keyPrefix().':detail:'.$item;
                        $score = self::getScoreValue($attribute,$this->hGet($modelKey,$attribute));
                        $this->zAdd($key,$score,$item);    
                    }
                    $result = $this->$method($key,0,-1);   
                }
            }            
            return $result;   
        }

        /**
        *  Chuyển đổi giá trị bất kì thành một số tương ứng để có thể sắp xếp đc trong kiểu sorted set của Redis
        *  @attribute string
        */
        public static function getScoreValue($attribute,$value){
            $return = 0;
            $typeAttribute = self::getTypeAttribute($attribute);
            switch ($typeAttribute) {
                case 'datetime':
                    $return = strtotime($value);
                    break;
                case 'integer':
                    $return = $value;
                    break;
                case 'float':
                    $return = $value;
                    break;
                case 'string':
                    $asciiValue = self::makeFriendlyString($value);
                    if ($asciiValue){
                        $length = strlen($asciiValue);
                        $convertedValue=0;
                        $numberOfCharsToConvert = 4;
                        $preFixNumber = 1000;
                        for ($i=0;$i<$numberOfCharsToConvert;$i++){
                            $charNumber = $preFixNumber;
                            if ($length > $i){
                                $charNumber = (ord(substr($asciiValue,$i,1))) * pow($preFixNumber,$numberOfCharsToConvert - 1 - $i); 
                            }
                            $convertedValue +=  $charNumber;  
                        }   
                        $return =  $convertedValue;      
                    }   
                    break;
                default:
                    break;
            }
            return $return;   
        }

        /**
        *   Trả về giá trị kiểu của thuộc tính, dựa trên khai báo trong rule, nếu ko thấy trả về giá trị string
        *  @attribute string
        */
        private static function getTypeAttribute($attribute) {
            $return = 'string';
            $className = get_called_class();
            $model = new $className();
            $rules = $model->rules();
            if ($rules && count($rules)) {
                foreach ($rules as $rule) {
                    if (in_array($attribute,$rule[0]) && !is_array($rule[1])){
                        switch ($rule[1]) {
                            case 'datetime':
                                $return = $rule[1];
                                break;
                            case 'integer':
                                $return = $rule[1];
                                break;
                            case 'float':
                                $return = $rule[1];
                                break;
                            default:
                                break;
                        }               
                    }
                }   
            }
            return  $return;
        }

        /** 
        * Chuyển đổi string unicode tiếng việt thành dạng ascii
        * @string string
        */
        private static function makeFriendlyString($string) {
            static $charMap = array(
                "à" => "a", "ả" => "a", "ã" => "a", "á" => "a", "ạ" => "a", "ă" => "a", "ằ" => "a", "ẳ" => "a", "ẵ" => "a", "ắ" => "a", "ặ" => "a", "â" => "a", "ầ" => "a", "ẩ" => "a", "ẫ" => "a", "ấ" => "a", "ậ" => "a",
                "đ" => "d",
                "è" => "e", "ẻ" => "e", "ẽ" => "e", "é" => "e", "ẹ" => "e", "ê" => "e", "ề" => "e", "ể" => "e", "ễ" => "e", "ế" => "e", "ệ" => "e",
                "ì" => 'i', "ỉ" => 'i', "ĩ" => 'i', "í" => 'i', "ị" => 'i',
                "ò" => 'o', "ỏ" => 'o', "õ" => "o", "ó" => "o", "ọ" => "o", "ô" => "o", "ồ" => "o", "ổ" => "o", "ỗ" => "o", "ố" => "o", "ộ" => "o", "ơ" => "o", "ờ" => "o", "ở" => "o", "ỡ" => "o", "ớ" => "o", "ợ" => "o",
                "ù" => "u", "ủ" => "u", "ũ" => "u", "ú" => "u", "ụ" => "u", "ư" => "u", "ừ" => "u", "ử" => "u", "ữ" => "u", "ứ" => "u", "ự" => "u",
                "ỳ" => "y", "ỷ" => "y", "ỹ" => "y", "ý" => "y", "ỵ" => "y",
                "À" => "A", "Ả" => "A", "Ã" => "A", "Á" => "A", "Ạ" => "A", "Ă" => "A", "Ằ" => "A", "Ẳ" => "A", "Ẵ" => "A", "Ắ" => "A", "Ặ" => "A", "Â" => "A", "Ầ" => "A", "Ẩ" => "A", "Ẫ" => "A", "Ấ" => "A", "Ậ" => "A",
                "Đ" => "D",
                "È" => "E", "Ẻ" => "E", "Ẽ" => "E", "É" => "E", "Ẹ" => "E", "Ê" => "E", "Ề" => "E", "Ể" => "E", "Ễ" => "E", "Ế" => "E", "Ệ" => "E",
                "Ì" => "I", "Ỉ" => "I", "Ĩ" => "I", "Í" => "I", "Ị" => "I",
                "Ò" => "O", "Ỏ" => "O", "Õ" => "O", "Ó" => "O", "Ọ" => "O", "Ô" => "O", "Ồ" => "O", "Ổ" => "O", "Ỗ" => "O", "Ố" => "O", "Ộ" => "O", "Ơ" => "O", "Ờ" => "O", "Ở" => "O", "Ỡ" => "O", "Ớ" => "O", "Ợ" => "O",
                "Ù" => "U", "Ủ" => "U", "Ũ" => "U", "Ú" => "U", "Ụ" => "U", "Ư" => "U", "Ừ" => "U", "Ử" => "U", "Ữ" => "U", "Ứ" => "U", "Ự" => "U",
                "Ỳ" => "Y", "Ỷ" => "Y", "Ỹ" => "Y", "Ý" => "Y", "Ỵ" => "Y"
            ); 
            return strtr($string, $charMap);
        }

        /**
        *  Trả về mảng primary key sau khi filter, sort , lấy limit
        */
        private function getData()
        { 
            if (($result = $this->getCacheData())===null) {
                $result = [];
                $filterAndWhere = $this->getDataByCondition($this->_condition);
                $filterOrWhere = $this->getDataByCondition($this->_orCondition,true);
                $primaryList = array_merge($filterAndWhere,$filterOrWhere);
                if (count($primaryList)) {
                    $result = array_values(array_flip(array_flip($primaryList)));
                    if ($this->_orderBy) {
                        if (count($this->_orderBy) > 1) throw new \Exception('Unsuppoted order by multi attributes');
                        else {
                            $attribute = array_keys($this->_orderBy)[0];
                            $sort = $this->_orderBy[$attribute];
                            $listAllSorted = $this->getSort($attribute,$sort); 
                            $resultWithSort = [];
                            foreach ($listAllSorted as $item) {
                                if (in_array($item,$result)) $resultWithSort[] = $item;
                            }
                            $result = $resultWithSort;                                          
                        }                     
                    }
                    if ($this->_limit){
                        $limit =  $this->_limit['limit'];
                        $offset =  ($this->_limit['offset']) ? $this->_limit['offset']:0;
                        $greater =  ($this->_limit['greater']) ? $this->_limit['greater']:true;
                        $resultWithLimit = [];
                        if ($greater) {
                            for ($i = $offset;$i < $offset + $limit;$i++ ){
                                if (isset($result[$i])) $resultWithLimit[] =  $result[$i];
                            }    
                        } else {                        
                            for ($i = $offset;$i > $offset - $limit;$i-- ){
                                if (isset($result[$i])) $resultWithLimit[] =  $result[$i];
                            }       
                        }
                        $result = $resultWithLimit;
                    }

                }
                $this->getCacheData($result);       
            }                              
            return $result; 
        }

        /**
        *  Trả về mảng primary key sau khi filter theo condition
        *  @condition Array
        */
        private function getDataByCondition($condition,$orType = false)
        { 
            $result = null;
            if (count($condition) > 0) {                
                foreach ($condition as $key => $item) {
                    $subResult = [];
                    $attribute = null;
                    $method = null;
                    $value = null;
                    if (is_array($item)) {
                        $method = $item[0];
                        $attribute = $item[1];   
                        $value = $item[2];
                    } else {
                        $attribute = $key;
                        $value = $item;
                    }
                    if ($this->hasAttribute($attribute)) {
                        $indexKey = $this->hGetAll(self::keyPrefix().':index:'.$attribute); 
                        if (!$indexKey) {
                            $allPrimary = $this->hGetAll(self::keyPrefix().':index:'.self::primaryKey());
                            if ($allPrimary){
                                foreach ($allPrimary as $item) {
                                    $modelKey = self::keyPrefix().':detail:'.$item;
                                    $attributeValue = $this->hGet($modelKey,$attribute);
                                    $this->hSet(self::keyPrefix().':index:'.$attribute,$item,$attributeValue);    
                                }
                                $indexKey = $this->hGetAll(self::keyPrefix().':index:'.$attribute); 
                            }
                        }
                        if ($indexKey) {
                            if (!$method) {
                                foreach ($indexKey as $keyIndex => $valueIndex) {
                                    if ($valueIndex == $value) $subResult[]= $keyIndex;
                                }    
                            }else{
                                $method = strtolower($method);
                                switch ($method){
                                    case '>':
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if ($valueIndex > $value) $subResult[]= $keyIndex;
                                        } 
                                        break;
                                    case '>=':
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if ($valueIndex >= $value) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case '<':
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if ($valueIndex < $value) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case '<=':
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if ($valueIndex <= $value) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case '==':
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if ($valueIndex == $value) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case '!=':
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if ($valueIndex != $value) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case 'in':
                                        $valueArr = explode(',',$value);
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if (in_array($valueIndex,$valueArr)) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case 'like':
                                        $firstChar = substr($value,0,1);
                                        $lastChar =  substr($value,strlen($value)-1,1);
                                        if (($firstChar == '%') && ($lastChar == '%')&&(strlen($value) > 2))
                                        {
                                            $newValue  = substr($value,1,strlen($value)-2);
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                if (strpos($valueIndex,$newValue)!==false) $subResult[]= $keyIndex;
                                            }
                                        }
                                        if (($firstChar != '%') && ($lastChar == '%')&&(strlen($value) > 1))
                                        {
                                            $newValue  = substr($value,0,strlen($value)-1);
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                if (strpos($valueIndex,$newValue)===0) $subResult[]= $keyIndex;
                                            }
                                        }
                                        if (($firstChar == '%') && ($lastChar != '%')&&(strlen($value) > 1))
                                        {
                                            $newValue  = substr($value,1,strlen($value)-1);
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                $valueIndex = $valueIndex.'|<>|';
                                                $subNewValue = $newValue.'|<>|';
                                                $posIndex = strpos($valueIndex,$subNewValue); 
                                                if (($posIndex !==false) && ($posIndex ==(strlen($valueIndex)-strlen($subNewValue)))) $subResult[]= $keyIndex;
                                            }
                                        }
                                        if (($firstChar != '%') && ($lastChar != '%')&&(strlen($value) > 0))
                                        {
                                            $newValue  = $value;
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                if ($valueIndex == $value) $subResult[]= $keyIndex;
                                            }
                                        } 
                                        break;
                                    case 'notlike':
                                        $likeResult = [];
                                        $firstChar = substr($value,0,1);
                                        $lastChar =  substr($value,strlen($value)-1,1);
                                        if (($firstChar == '%') && ($lastChar == '%')&&(strlen($value) > 2))
                                        {
                                            $newValue  = substr($value,1,strlen($value)-2);
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                if (strpos($valueIndex,$newValue)!==false) $likeResult[]= $keyIndex;
                                            }
                                        }
                                        if (($firstChar != '%') && ($lastChar == '%')&&(strlen($value) > 1))
                                        {
                                            $newValue  = substr($value,0,strlen($value)-1);
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                if (strpos($valueIndex,$newValue)===0) $likeResult[]= $keyIndex;
                                            }
                                        }
                                        if (($firstChar == '%') && ($lastChar != '%')&&(strlen($value) > 1))
                                        {
                                            $newValue  = substr($value,1,strlen($value)-1);
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                $valueIndex = $valueIndex.'|<>|';
                                                $subNewValue = $newValue.'|<>|';
                                                $posIndex = strpos($valueIndex,$subNewValue); 
                                                if (($posIndex !==false) && ($posIndex ==(strlen($valueIndex)-strlen($subNewValue)))) $likeResult[]= $keyIndex;
                                            }
                                        }
                                        if (($firstChar != '%') && ($lastChar != '%')&&(strlen($value) > 0))
                                        {
                                            $newValue  = $value;
                                            foreach ($indexKey as $keyIndex => $valueIndex) {
                                                if ($valueIndex == $value) $likeResult[]= $keyIndex;
                                            }
                                        }
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if (!in_array($keyIndex,$likeResult)) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    case 'between':
                                        $valueFrom = $value[0];
                                        $valueTo = $value[1];
                                        foreach ($indexKey as $keyIndex => $valueIndex) {
                                            if (($valueIndex >= $valueFrom) &&($valueIndex <= $valueTo)) $subResult[]= $keyIndex;
                                        }
                                        break;
                                    default:
                                        throw new \Exception('Unsuppoted method: '.$method);
                                        break;
                                }   
                            }
                        }
                    } 
                    if ($result === null) $result = $subResult;
                    else {
                        $tmpResult = [];
                        if (count($subResult)) 
                            foreach ($subResult as $subValue) {
                                if (in_array($subValue,$result)) $tmpResult[] = $subValue;
                        }
                        $result = $tmpResult;
                    }                  
                }
            }else{
                if (!$orType){
                    $allPrimary = $this->hGetAll(self::keyPrefix().':index:'.self::primaryKey());
                    if ($allPrimary) return $allPrimary;   
                }  
            }
            return  $result ? $result:[];        
        } 

        /**
        *  Cache kết quả câu query
        *  @data Array | Object
        */
        private function getCacheData($data=null)
        {             
            if  ($this->_cached) {
                $querry = ['condition'=>$this->_condition,'orCondition'=>$this->_orCondition,'limit'=>$this->_limit,'orderBy'=>$this->_orderBy];
                $keyCache = self::keyPrefix().":cached:".base64_encode(json_encode($querry));
                if ($data) {
                    $this->set($keyCache,json_encode($data));
                    return $data;   
                } 
                else {
                    $data = $this->get($keyCache);
                    if ($data) return json_decode($data, true);
                    else  $this->set($keyCache,json_encode([]));  
                } 
            }
            return null;          
        }     
}