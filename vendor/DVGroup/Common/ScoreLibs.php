<?php
namespace DVGroup\Common;
use DVGroup\Redis\Redis;
use DVGroup\Db\Model\BaseTable;
use Zend\Json\Json;

class ScoreLibs {

    protected static $redis;
    public static  function getRedis(){
        if(!self::$redis)
            self::$redis = new Redis();
        return self::$redis;
    }

    /****  Tính điểm ---------*/

    /**
     * Tinh diem trung binh do cong dong cham
     * Co id cua bai hat => duyet danh sach user cham diem bai hat do
     * Co id cua user cham diem bai hat => co so diem, va trong so cua uuser
     * groupid = 1 superadmin
     * groupid = 2 admin
     * groupid = 0 user
     * Neu chi co mot nhom cham diem thi he so giu nguyen
     * Diem do supper admin cham thi co he so nhan 2
     */
    public static  function getAvgScoreMusic($sid)
    {
        $redis       = self::getRedis();
        $user_score  = [];
        $admin_score = [];
        $user_scale  = Params::getParams('USER_SCALE');
        $user_factor = Params::getParams('USER_FACTOR');
        $scores = $redis->sort('score:music_who_score_this_song:' . $sid, [
            'by'  => 'score:score_of_music:a:*->*',
            'get' => ['#','score:score_of_music:a:*->' . $sid],
        ]);
        $scores = CommonLibs::sortResult($scores); //sắp xếp lại điểm cho từng user
        if (sizeof($scores)) {
            foreach ($scores as $user => $score) {
                $uid     = $redis->hGet('tb_user_rd_username_id', $user);
                $groupid = $redis->hGet('tb_user_rd:a:' . $uid, 'group');
                if ($groupid > 0) {
                    $admin_score[$user]['score']  = $score;
                    $admin_score[$user]['factor'] = isset($user_factor[$groupid]) ? $user_factor[$groupid] : '';
                    $admin_score[$user]['scale']  = isset($user_scale[$groupid]) ? $user_scale[$groupid] : '';
                } else {
                    $user_score[$user]['score']  = $score;
                    $user_score[$user]['factor'] = isset($user_factor[$groupid]) ? $user_factor[$groupid] : '';
                    $user_score[$user]['scale']  = isset($user_scale[$groupid]) ? $user_scale[$groupid] : '';
                }
            }
        }
        return self::caculateAvg($user_score, $admin_score);
    }

    //Tính điểm bạn bè
    public static function getAvgScoreMusicByFriends($mid, $username)
    {
        $redis = self::getRedis();
        $key   = 'tb_user_friends_rd:' . $username;
        $data  = $redis->sort($key, [
            'by'  => 'score:score_of_music:a:*->' . $mid,
            'get' => 'score:score_of_music:a:*->' . $mid
        ]);
        return self::averageScore($data);
    }

    /**
     * Tinh diem trung binh
     */
    public static function caculateAvg($user_score = null, $admin_score = null)
    {
        $count_u    = 0;
        $total_u    = 0;
        $avg_u      = 0;
        $user_scale = Params::getParams('USER_SCALE');
        if (count($user_score)) {
            foreach ($user_score as $ar) {
                $total_u += $ar['score'];
                $count_u++;
            }
        }
        if ($count_u) {
            $avg_u = $total_u / $count_u;
        }

        $count_a = 0;
        $total_a = 0;
        $avg_a   = 0;

        if (count($admin_score)) {
            foreach ($admin_score as $ar) {
                $total_a += $ar['score'] * $ar['factor'];
                $count_a += $ar['factor'];
            }
        }
        if ($count_a) {
            $avg_a = $total_a / $count_a;
        }
        if (count($user_score) && count($admin_score)) {
            $avg_a = $avg_a * $user_scale[1];
            $avg_u = $avg_u * $user_scale[0];
        }
        $total_avg = $avg_a + $avg_u;

        return $total_avg;
    }

    /* Tinh diem trung binh cua bai hat
    * $arr la bien mang tham chieu giua nguoi cham va so diem phan tu thu nhat la nguoi cham,
    * phan tu thu hai la so diem
    */

    public static function averageScore($arr)
    {
        $c     = 0;
        $total = 0;
        foreach ($arr as $value) {
            if ($value) {
                $c++;
                $total += $value;
            }
        }
        $av = $c ? $total / $c : 0;
        return $av;
    }

    const  EXPIRE_CACHE = 7200;

    /*
  * TINH DIEM TREN REDIS
  * Lay danh sach ban be tb_user_friends_rd
  * Tong hop danh sach cac bai hat duoc cham boi ban be tb_music_list_score_by_user_rd
  * For tinh diem tung bai hat theo cau lenh duoi
  * sort  tb_user_friends_rd:duongtt by tb_music_score_music_rd:a:*->2014080418343024
  * get tb_music_score_music_rd:a:*->2014080418343024
  *
  */

    public static function getSuguestMusic($username, $limit)
    {
        $redis       = self::getRedis();
        $result      = [];
        $resultTotal = [];
        $keyFriend   = 'tb_user_friends_rd:' . $username;
        /** lay danh sach ban be*/
        $friendslist = $redis->sGetMembers($keyFriend);
        $arg1[]      = 'score:list_music:' . $username;
        $arg1[]      = sizeof($friendslist);
        if (count($friendslist) > 0 && is_array($friendslist)) {
            foreach ($friendslist as $value) {
                $arg1[] = 'score:music_list_score_by_user:' . $value;
            }
        }
        $redis->zUnion('out', $arg1);
        /** Tong hop danh sach bai hat cua cac friend cham diem*/
        $listAllMusic = $redis->zRange('score:list_music:' . $username, 0, -1);
        /*  * Diem cua bai hat do cac friend cham*/
        if (is_array($listAllMusic) && count($listAllMusic) > 0) {
            foreach ($listAllMusic as $value) {
                $key          = 'tb_user_friends_rd:' . $username;
                $data         = $redis->sort($key, [
                    'by'  => 'score:score_of_music:a:*->' . $value,
                    'get' => 'score:score_of_music:a:*->' . $value
                ]);
                $averageScore = ScoreLibs::averageScore($data);
                if ($averageScore > 0) {
                    $redis->zAdd('score:list_music_suguest:' . $username, $averageScore, $value);
                }
            }
        }
        return $redis->zRevRangeByScore('score:list_music_suguest:' . $username, '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, $limit)));
    }

    /*
     * Lay danh sach diem cao nhat do cong dong cham
     * Tra ve mot list danh sach gom diem va id bai hat
     */
    public static function getListMaxScoreMusic($limit)
    {
        $redis = self::getRedis();
        return $redis->zRevRangeByScore('score:list_music_average_score', '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, $limit)));
    }

    /*lấy thông tin bài hát theo identify*/

    public static function getMusicRdByIdentify($identify)
    {
        $redis = self::getRedis();
        return $redis->hGetAll('tb_music_title_foread_rd:a:' . $identify);
    }

    /**
     * lay diem cua bai hat
     */
    public static function getOwnerScore($key, $username)
    {
        $redis = self::getRedis();
        return $redis->hGet('score:score_of_music:a:' . $username, $key);
    }

    /*
       * Get diem do user cham mot bai hat
       * dau vao la id bai hat,  username
       * Tra ve so diem
       */

    public static function getScoreOfUser($username, $sid)
    {
        $redis = self::getRedis();
        return $redis->hGet('score:score_of_music:a:' . $username, $sid);
    }

    /**
     * Lay tong so lan da cham diem trong khoang thoi gian quy dinh
     */
    public static function getLastActionScore($username)
    {
        $redis = self::getRedis();
        return $redis->get('record_action_score:' . $username);
    }

    /**
     * setQueue lưu thông tin điểm cần insert vào mysql trong một queue,
     * sử dung crontab để định kì quét trong queue xem có bản ghi nào đang chờ để ghi vào mysql hay không
     */
    public static function insertQueue($score, $identify, $username, $sl)
    {
        $QUEUE             = 'queue_rd:a:';
        $QUEUE_LAST_INSERT = 'queue_rd:s:id';

        $redis   = self::getRedis();
        $last_id = $redis->get($QUEUE_LAST_INSERT);
        $last_id++;
        $redis->set($QUEUE_LAST_INSERT, $last_id, $expire = 7200); //save last id
        $ob                = (object)array('score' => $score, 'identify' => $identify, 'username' => $username, 'sl' => $sl);
        $args              = array();
        $args['time']      = time();
        $args['propeties'] = Json::encode($ob);
        $args['id']        = $last_id;
        $key               = $QUEUE . $last_id;
        $args['name']      = 'score';
        $redis->hmset($key, $args);
    }

    /*
* add mot bai hat vao list danh sach bai hat duoc cham boi username
* @param $username : ten user cham bai hat
* @param $sid: id bai hat duoc cham boi $username
*
*/
    public static function insertListMusicScoreByUser($username, $sid)
    {
        $redis = self::getRedis();
        $key   = 'score:music_list_score_by_user:' . $username;
        $redis->zAdd($key, time(), $sid);
        $redis->sAdd('score:music_users_list:' . $sid, $username); //so nguoi cham diem cho bai hat
        $redis->rPush('score:music_who_score_this_song:' . $sid, $username); //so nguoi cham diem cho bai hat cộng đồng
    }

    /*
 * add mot hash luu diem cua moi bai hat duoc cham boi username
 * @param $username : ten user cham bai hat
 * @param $sid: id bai hat duoc cham boi $username
 * @param $score: diem cua bai hat
 * @param $type loai bai hat la video hay mp3 type 1 la video
 */

    public static function insertScoreMusic($username, $sid, $score, $type = IS_MUSIC)
    {
        $redis     = self::getRedis();
        $timestamp = time();
        /*
         * Luu diem vao hash moi user co mot hash chua danh sach cac bai hat va diem cham
         */
        $key = 'score:score_of_music:a:' . $username;
        $redis->hSet($key, $sid, $score);
        /*
         * Lay user groupid
         */
        $uid     = $redis->hGet('tb_user_rd_username_id', $username);
        $groupid = $redis->hGet('tb_user_rd:a:' . $uid, 'group');
        /*
         * Lay thong so cau hinh
         */
        $user_factor = Params::getParams('USER_FACTOR');
        $user_scale  = Params::getParams('USER_SCALE');
        /*
         * Luu tong diem vao mot sort set tb_music_total_score_rd:idbaihat timestamp tongdiem
         */
        //Lay tong diem hien tai
        $curentTotalScore_user    = 0;
        $curentNumberPeople_user  = 0;
        $curentTotalScore_admin   = 0;
        $curentNumberPeople_admin = 0;
        $newTotalScore_user       = 0;
        $newNumberPeople_user     = 0;
        $newTotalScore_admin      = 0;
        $newNumberPeople_admin    = 0;
        $avg_score_user           = 0;
        $avg_score_admin          = 0;
        if (!$groupid) {
            //Truong hop nguoi cham la user binh thuong, chi tinh lai diem do user cham
            //Diem do admin cham thi khong can tinh lai, chi can lay tu redis ra
            $curentTotalScore_user = self::getTotalScoreOfUserThisSong($sid);
            //Cong tong diem hien tai va diem vua duoc cham => tong diem moi
            $newTotalScore_user = $curentTotalScore_user + $score;
            //Cap nhat len redis
            $redis->zAdd('score:music_total_score_user:' . $sid, $timestamp, $newTotalScore_user);
            //Lay tong so nguoi cham hien tai
            $curentNumberPeople_user = self::getTotalUserScoreThisSong($sid);
            $newNumberPeople_user    = $curentNumberPeople_user + 1;
            $redis->zAdd('score:music_total_people_user:' . $sid, $timestamp, $newNumberPeople_user);
            $avg_score_user = $newTotalScore_user / $newNumberPeople_user;
            //Lay tong diem do admin cham tu redis
            $curentTotalScore_admin = self::getTotalScoreOfAdminThisSong($sid);
            //Lay tong so nguoi cham diem bai hat nay tu redis
            $curentNumberPeople_admin = self::getTotalAdminScoreThisSong($sid);
            $avg_score_admin          = $curentNumberPeople_admin ? $curentTotalScore_admin / $curentNumberPeople_admin : 0;

        } else {
            //Truong hop user la admin, superadmin => tinh lai diem so do cac admin cham
            //Lay diem so do user cham da luu truoc do khong can tinh lai
            $curentTotalScore_admin = self::getTotalScoreOfAdminThisSong($sid);
            //Cong tong diem hien tai va diem vua duoc cham => tong diem moi
            $newTotalScore_admin = $curentTotalScore_admin + $score * (isset($user_factor[$groupid]) ? $user_factor[$groupid] : 1);
            //Cap nhat len redis
            $redis->zAdd('score:music_total_score_admin:' . $sid, $timestamp, $newTotalScore_admin);
            //Lay tong so nguoi cham hien tai
            $curentNumberPeople_admin = self::getTotalAdminScoreThisSong($sid);
            $newNumberPeople_admin    = $curentNumberPeople_admin + (isset($user_factor[$groupid]) ? $user_factor[$groupid] : 1);
            $redis->zAdd('score:music_total_people_admin:' . $sid, $timestamp, $newNumberPeople_admin);
            $avg_score_admin = $newTotalScore_admin / $newNumberPeople_admin;
            //Lay diem do user cham tu redis
            $curentTotalScore_user = self::getTotalScoreOfUserThisSong($sid);
            //Lay tong so user da cham bai hat tu redis
            $curentNumberPeople_user = self::getTotalUserScoreThisSong($sid);
            $avg_score_user          = $curentNumberPeople_user ? $curentTotalScore_user / $curentNumberPeople_user : 0;
        }

        /*
         * Luu diem trung binh hien tai cua bai hat tb_music_average_score:$sid
         */
        //$avgscore=$newTotalScore/$newNumberPeople;
        if ($avg_score_user && $avg_score_admin) {
            $avg_total = $avg_score_user * $user_scale[0] + $avg_score_admin * $user_scale[1];
        } else {
            $avg_total = $avg_score_user + $avg_score_admin;
        }
        $avgscore = self::getAvgScoreMusic($sid);
        $redis->zAdd('score:music_average_score:' . $sid, $timestamp, $avgscore);
        /*
         * Luu danh sach diem trung binh cua tat ca cac bai hat duoc cham boi cong dong
         */
        if ($type) {
            $redis->zAdd('score:list_video_average_score', $avgscore, $sid);
        } else {
            $redis->zAdd('score:list_music_average_score', $avgscore, $sid);
        }
    }

    /*
* Ham lay so diem cham boi user
*/

    public static function getTotalScoreOfUserThisSong($sid)
    {
        $redis               = self::getRedis();
        $scoreMusicTotalUser = $redis->zRevRangeByScore('score:music_total_score_user' . $sid, '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, 1)));
        if ($scoreMusicTotalUser)
            return $scoreMusicTotalUser[0];
        else
            return 0;
    }

    /**
     * Lay tong so user da cham
     */
    public static function getTotalUserScoreThisSong($sid)
    {
        $redis                 = self::getRedis();
        $scoreMusicTotalpeople = $redis->zRevRangeByScore('score:music_total_people_user' . $sid, '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, 1)));
        if ($scoreMusicTotalpeople)
            return $scoreMusicTotalpeople[0];
        else
            return 0;
    }

    /*
   * Ham lay tong diem da cham boi admin
   */

    public static function getTotalScoreOfAdminThisSong($sid)
    {
        $redis                = self::getRedis();
        $scoreMusicTotaladmin = $redis->zRevRangeByScore('score:music_total_score_admin' . $sid, '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, 1)));
        if ($scoreMusicTotaladmin) {
            return $scoreMusicTotaladmin[0];
        } else {
            return 0;
        }
    }

    /**
     * Lay tong so admin da cham
     */
    public static function getTotalAdminScoreThisSong($sid)
    {
        $redis                       = self::getRedis();
        $scoreMusicTotalpeople_admin = $redis->zRevRangeByScore('score:music_total_people_admin' . $sid, '+inf', '-inf', array('withscores' => TRUE, 'limit' => array(0, 1)));
        if ($scoreMusicTotalpeople_admin) {
            return $scoreMusicTotalpeople_admin[0];
        } else {
            return 0;
        }
    }

    /**
     * RecordActionScore Lưu lại hành động chấm điểm của user trong khoảng thời gian nhất đinh
     * Thời gian lấy từ cấu hình params
     *
     */
    public static function recordActionScore($username) {
        $redis = self::getRedis();
        $curentTimeStamp = time();
        $conf = Params::getParams('limitAction');
        $timeLimit = $curentTimeStamp + $conf['time'];

        if (!$redis->exists('record_action_score:' . $username)) {
            $redis->incr('record_action_score:' . $username);
            $redis->expireAt('record_action_score:' . $username, $timeLimit);
        } else {
            $redis->incr('record_action_score:' . $username);
        }
    }

    /*
* Lay danh sach user cham bai hat co id la $mid
* Dau vao la id bai hat, so user can lay
* dau ra la list user
*/

    public static function whoScoreThisSog($mid, $number) {
        $redis = self::getRedis();
        return $redis->lRange('score:music_who_score_this_song:'.$mid , 0, $number);
    }


    /*END Tính điểm*/
}
