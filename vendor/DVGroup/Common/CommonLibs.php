<?php
namespace DVGroup\Common;

use DVGroup\Auth\AuthUser;
use DVGroup\Redis\Redis;
use DVGroup\Common\Utf8;

class CommonLibs {

    public static function params()
    {
        return include APPLICATION_PATH. '/config/autoload/params.php';
    }
    //lay adapter
    public static function getAdapter()
    {
        //$config=include APPLICATION_PATH. '/config/autoload/params.php';
        $k1=include APPLICATION_PATH.'/config/autoload/global.php';
        $k2=include APPLICATION_PATH.'/config/autoload/local.php'; 
        $config=array_merge_recursive($k1,$k2); 
        $adapter=new \Zend\Db\Adapter\Adapter($config['db']);
        return $adapter;        
    }
    public static function getAllConfig()
    {
        $k=glob(APPLICATION_PATH.'/module/*/config/module.config.php');
        $arr=[];
        foreach ($k as $val)
        {
             $l= include $val;
             $arr=array_merge_recursive($arr,$l);    
        }
        return $arr;
    }
    //tra lai url theo params, giong this->url() o view
    public static function url($routename='home',$params=[])
    {
        $part=explode('/',$routename);
        $allcf=self::getAllConfig();
        $config=$allcf['router']['routes'][$part[0]]['options'];
        $route=$config['route'];
        $constraints=$config['constraints'];
        $defaults=$config['defaults'];        
        $serg= new \Zend\Mvc\Router\Http\Segment($route,$constraints,$defaults);
        $options=$allcf['router']['routes'][$part[0]]['child_routes'];
        $path=$serg->assemble($params);
        $lenght=sizeof($part);
        for($i=1;$i<$lenght;$i++)
        {
            
            $path.=(new \Zend\Mvc\Router\Http\Segment($options[$part[$i]]['options']['route'],$constraints,$defaults))->assemble($params);
        }
        return $path;
    }
    protected static $redis;
    public static  function getRedis(){
        if(!self::$redis)
            self::$redis = new Redis();
        return self::$redis;
    }
//limit string
    public static function text_limit($string,$len)
    {
        if($len > strlen($string)){$len=strlen($string);};
        $pos = strpos($string, ' ', $len);
        if($pos){$string = substr($string,0,$pos);}else{$string = substr($string,0,$len);}
        return $string;
    }
//    end
    public static function shortern_word($text, $length) {
		if (strlen($text) > $length) {
			$text = substr($text, 0, strpos($text, ' ', $length));
		}
		return $text;
	}

	static function startsWith($haystack, $needle) {
		return !strncmp($haystack, $needle, strlen($needle));
	}

	static function endsWith($haystack, $needle) {
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}
		return (substr($haystack, -$length) === $needle);
	}

	public static function Zip($obj) {
		return gzcompress(base64_encode(json_encode($obj)), 9);
	}

	public static function UnZip($obj) {
		//return CommonLibs::ConvertObjectToArray(json_decode(base64_decode(gzuncompress($obj))));
		//	var_dump($obj);
		if ($obj != false)
			return json_decode(base64_decode(gzuncompress($obj)), true);
		else
			return NULL;
	}

	// @tienhvt2: tach chuoi title thanh tieu de va ten nghe si
	// quangkh tam thoi hide di - tim kiem cho nao su dung thi sua lai
// 	public static function convertTitleMusic($title) {
// 		$pos = strpos($title, '-');
// 		$tit = trim(substr($title, 0, $pos));
// 		$artist = trim(substr($title, $pos + 1));
// 		return array('title' => $tit, 'artist' => $artist);
// 	}
	
	
	/**
	 * QuangKH - Ham chuyen doi URL
	 * @param string $text
	 * @return string
	 */
	public static function GenRawUrl($text) {
		// quangkh doi lai sang ham UTF8 xu ly URL dep
		$utf8 = new utf8();
		$text = $utf8->urlHelper($text);
		return $text;
	}
	
	/**
	 * QuangKH
	 * Hàm xử lý trả về link profile của nghệ sĩ
	 * @name Ten cua nghe si
	 * @namespace Duong dan SLUG url
	 * return URL
	 */
	public static function genUrlArtist($name, $name_search, $tabName = null, $sortBy = NULL) {
		//     	use app\helpers\utf8;
		$name_search = str_replace([' ', '.'], ['-', ''], trim($name_search));
        
        
		if ($tabName != null && $sortBy == null) {
			$url = '/nghe-si/' . $name_search . '.' . $tabName . '.html';
		} elseif ($tabName != null && $sortBy != null) {
			$url = '/nghe-si/' . $name_search . '.' . $tabName . '.' . $sortBy . '.html';
		} else {
			$url = '/nghe-si/' . $name_search . '.songslist.html';
		}
		return $url;
	}
	
	public static function getUrlArtist($name, $nam_search) {
		$slug = self::GenRawUrl(trim($nam_search));
		$text = self::genTagUrlArtist($name, strtolower($slug)) . ', ';
		$text = substr($text, 0, -2);
		return ['title' => $name, 'artists' => $text];
	}
	
	/**
	 * QuangKH
	 * Hàm xử lý trả về TAG A link profile của nghệ sĩ
	 * @name Ten cua nghe si
	 * @namespace Duong dan SLUG url
	 * return html <a ....>...</a>
	 */
	public static function genTagUrlArtist($name, $name_search) {
		//     	use app\helpers\utf8;
		$name_search = str_replace(' ', '-', trim($name_search));
        $url=self::url('ArtistDetail',['name'=>$name_search]);
	//	$url = '/nghe-si/' . $name_search;
		$text = '<a style="color: #333534" href="' . $url . '" title="' . Utf8::cleanTitle($name) . '">' . $name . '</a>';
		return $text;
	}
	
	/**
	 * QuangKH hàm xử lý Tên bài hát có kèm tên ca sĩ và trả về mảng Tên bài hát, Tên ca sĩ, link ca sĩ
	 * @title Tên bài hát kèm Tên ca sĩ
	 * @return multitype:
	 */
	public static function convertTitleMusic($title) {
		// truong hop fix cung dac biet
		if (substr_count($title, "M-TP") > 0) {
			$artist = str_replace("-", "", strrchr($title, "Sơn Tùng"));
		} else {
			$artist = str_replace("-", "", strrchr($title, "-"));
		}
		$text = '';
		// dinh nghia lai kieu phan cach giua cac ca si trong bai hat
		$artist = str_replace([' ft. ', ' ft ', ' if '], ',', $artist);
	
		$artists = explode(',', $artist);
		if ($artists && count($artists) > 0) {
			foreach ($artists as $val) {
				$slug = '';
				$slug = self::GenRawUrl(trim($val));
				$text .= self::genTagUrlArtist($val, strtolower($slug)) . ', ';
			}
			$text = substr($text, 0, -2);
		}
		
		if (substr_count($title, '-') > 0) {
			$title = substr(str_replace(array($artist, '-', '%20'), array("", "", ' '), $title), 0, -1);
		}
	
		return ['title' => $title, 'artists' => $text];
	}
	
	/**
	 * Hàm convert title cho Album Playlist
	 * @param string $title
	 * @return array title và ca sĩ
	 */
	public static function convertTitlePlaylist($title) {
		$artist = str_replace("-", "", strrchr($title, "-"));
		$text = '';
		$artists = explode(',', $artist);
		if ($artists && count($artists) > 0) {
			foreach ($artists as $val) {
				$slug = '';
				$slug = self::GenRawUrl(trim($val));
				$text .= $val . ', ';
			}
			$text = substr($text, 0, -2);
		}
	
		$title = substr(str_replace($artist, "", $title), 0, -1);
	
		return ['title' => $title, 'artists' => $text];
	}

	// @tienhvt2: ham nay tra ve gia tri random cua danh muc video trong truong hop ko tim thay danh muc bai hat tuong ung
	public static function random_cate($tmp_cat, $id) {
		$tmp_arr = array();
		foreach ($tmp_cat as $key => $val) {
			if (isset($tmp_cat[$id]['slug'])) {
				if ($val['slug'] == 'video-' . $tmp_cat[$id]['slug']) {
					return $key;
				}

			}
			if (strpos($val['slug'], 'video') !== false) {$tmp_arr[] = $key;
			}
		}
		$kk = sizeof($tmp_arr);
		return $tmp_arr[rand(0, $kk)];
	}

	public static function random_m_cate($tmp_cat, $id) {
		$tmp_arr = array();
		foreach ($tmp_cat as $key => $val) {
			if (isset($tmp_cat[$id]['slug'])) {
				if ('video-' . $val['slug'] == $tmp_cat[$id]['slug']) {
					return $key;
				}

			}
			if (strpos($val['slug'], 'video') == false) {$tmp_arr[] = $key;
			}
		}
		$kk = sizeof($tmp_arr);
		return $tmp_arr[rand(0, $kk)];
	}

    public static function chuyenKhongdau2($txt) {
        $arraychar = array(
            array("đ", "Đ"),
            array("ó", "ỏ", "o", "ò", "o", "ọ", "Ọ", "o", "õ", "ô", "Ô", "ỗ", "ổ", "ồ", "ố", "ộ", "ơ", "ỡ", "ớ", "ờ", "ở", "ợ"),
            array("ì", "í", "ỉ", "ì", "ĩ", "ị"),
            array("ê", "Ê", "ệ", "ế", "ể", "ễ", "ề", "é", "ẹ", "ẽ", "è", "ẻ"),
            array("ả", "á", "Á", "ạ", "ã", "à", "â", "Â", "ẩ", "ấ", "ầ", "ậ", "ẫ", "ă", "ẳ", "ắ", "ằ", "ặ", "ẵ"),
            array("ũ", "ụ", "ú", "ủ", "ù", "ư", "ữ", "ự", "ứ", "ử", "ừ", "Ư", "Ú"),
            array("ỹ", "ỵ", "ý", "ỷ", "Ỷ", "ỳ", "Ý", "Ỷ")
        );

        $arrayconvert = array("d", "o", "i", "e", "a", "u", "y");

        for ($i = 0; $i < sizeof($arraychar); $i++) {
            foreach ($arraychar[$i] as $key => $value) {
                $txt = str_replace($value, $arrayconvert[$i], $txt);
            }
        }

        //return urlencode(strtolower($txt));
        return (mb_strtolower($txt));
    }

	public static function convet_to_slug($txt) {
		$txt = self::chuyenKhongdau2($txt);
		$patern = '[\W+]';
		return preg_replace($patern, '-', $txt);
	}
    public static function TakeKey($length=16)
    {
        $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ123456789";
        $validCharNumber = strlen($validCharacters);
        $result = "";
        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }
        return $result;
    }

    //HAIHS

    /**
     * Sap xep lai ket qua tra ve tu redis tu dang aray thanh object
     * @param Dau vao la mang du lieu tra ve tu redis $arr
     * @return Mot mang gia tri kieu key value
     */
    public static function sortResult($arr) {
        $result = [];
        for ($i = 0; $i < sizeof($arr); $i = $i + 2) {
            $result[$arr[$i]] = $arr[$i + 1];
        }
        return $result;
    }

    /*Xử lý chuỗi trả về 1 slug */
    public static function slug_title($str) {
        $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
        $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
        $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
        $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
        $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
        $str = preg_replace("/(đ)/", 'd', $str);
        $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
        $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
        $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
        $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
        $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
        $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
        $str = preg_replace("/(Đ)/", 'D', $str);
        //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $str))) ;
    }

   	public static function getPlaylistImage($resource = '', $name_search = '', $cate_id,$server = 'http://video.kenhtin.net') {
		return $server . '/music_file/images/playlist/'.$cate_id.'/' . str_replace(' ', '-', $name_search) . '/' . $resource;
	}

    #    Output easy-to-read numbers
    #    by james at bandit.co.nz

    public static function bd_nice_number($n) {
        // first strip any formatting;
        $n = (0 + str_replace(",", "", $n));

        // is this a number?
        if (!is_numeric($n))
            return false;

        // now filter it;
        if ($n > 1000000000000)
            return round(($n / 1000000000000), 0) . ' trillion';
        else if ($n > 1000000000)
            return round(($n / 1000000000), 0) . ' billion';
        else if ($n > 1000000)
            return round(($n / 1000000), 0) . 'M';
        else if ($n > 10000)
            return round(($n / 1000), 0) . 'K';
//   	else if($n>1000) return round(($n/1000),1).' thousand';
        else if ($n > 1000)
            return number_format($n);


        return number_format($n);
    }
    /**
     * @author KienNT
     * return artist avatar or banner image 
     * @param string $resource
     * @param string $name_search
     * @param string $folder
     * @param number $cate_id
     * @return string
     */
    
    private static $media_server = 'http://video.kenhtin.net';
    public static function getUrlImageArtist($resource = '', $name_search = '', $folder = 'avatar', $cate_id = 0) {
    	if ($cate_id && $cate_id > 0) {
    		$imageUrl = CommonLibs::$media_server . '/music_file/images/' . $folder . '/' . $cate_id . '/' . str_replace ( ' ', '-', $name_search ) . '/' . $resource;
    	} else {
    		$imageUrl = CommonLibs::$media_server . '/music_file/images/' . $folder . '/' . str_replace ( ' ', '-', $name_search ) . '/' . $resource;
    	}
    	return $imageUrl;
    }
    
    /**
     * @author KienNT
     * @param string $resource
     * @param string $name_search
     * @return string
     */
    public static function getAlbumImage($resource = '', $name_search = '') {
    	return CommonLibs::$media_server . '/music_file/images/album/' . str_replace ( ' ', '-', $name_search ) . '/' . $resource;
    }
    /**
     * @author KienNT
     * Get audio or video real-path
     * @param unknown $resource
     * @param unknown $cate_key
     * @param number $type
     * @param unknown $artist_id
     * @param unknown $identify
     * @param string $remoteIp
     * @param number $expires
     * @return string
     */
    public static function getRealMediaPath($resource, $cate_key, $type = 0, $artist_id, $identify, $remoteIp = NULL, $expires = 0) {
    	$folder = "audio";
    	$key = '';
    	if ($type == 1) {
    		$folder = "video";
    		$key = 'video-';
    		$file = '/music_file/files/' . $folder . '/' . str_replace ( "video-", "", $cate_key ) . '/' . $artist_id . '/' . $identify . '/' . $resource;
    		$ip = self::params()['IP_Video_Sv'];
    		$file = CommonLibs::generateUrl ( $file, $ip );
    	} else {
    		$file = CommonLibs::$media_server . '/music_file/files/' . $folder . '/' . str_replace ( "video-", "", $cate_key ) . '/' . $artist_id . '/' . $identify . '/' . $resource;
    	}
    	return $file;
    }
    public static function generateUrl($resource, $remoteIp, $expires = 0) {
       
        $expires = $expires <= 0 ? time() + 30 * 60 : $expires;
        $secret = self::params()['SECRET'];
        $host = self::params()['HOST'];
        $format =self::params()['FORMAT']. '%s?token=%s&expires=%d';
        $token = base64_encode(md5($expires . $resource . $remoteIp . $secret, true));
        $token = str_replace('+', '-', $token);
        $token = str_replace('=', '', $token);
        $token = str_replace('/', '_', $token);
        return sprintf($format, $resource, $token, $expires);
    }
    

    public static function getAvatarDefault($type = '') {
        if ($type == 1)
            return '/theme/default/img/default_banner_artist.jpg';
        return 'images/avata/default.gif';

    }

    /**
     * function get avatar
     * 28/07/2014 linhnt edit return value
     * @param $thumb string (truyen 2 thong tin username va size thumb ex: abc_50 )
     * @param $logged boolean (true: neu param truyen vao  == '' thi lay thong tin tu user current; false: chi lay param truyen vao, neu ko return default
     */
    public static function getAvatar($path = '', $avatar = '', $logged = false, $thumb = '') {

        if ($logged){
            $login    = new AuthUser();
            $user     = $login->getUser();
            if (strlen($user->avatar_path) > 0 && strlen($user->avatar) > 0) {
                $file_ext = strtolower(substr($user->avatar, strrpos($user->avatar, '.') + 1));
                if (file_exists($user->avatar_path . $user->avatar)) {
                    $thumb = $user->avatar_path . 'thumbnail_' . $user->username . "." . $file_ext;
                    if (file_exists($thumb)) {
                        return '/' . $thumb;
                    }
                }
            }
        }
        if ($path != '' && $avatar != '' && $thumb != '') {
            $file_ext = strtolower(substr($avatar, strrpos($avatar, '.') + 1));
            $size_thumb = strrpos($thumb, '_') ? substr($thumb, strrpos($thumb, '_') + 1) : '';
            $thumbname = str_replace('_' . $size_thumb, '', $thumb);
            $file_path = $path . 'thumbnail_' . $thumbname . '.' . $file_ext;
            if (file_exists($file_path)) {
                return '/' . $file_path;
            }
        } elseif ($path != '' && $avatar != '' && $thumb == '') {
            if (file_exists($path . $avatar)) {
                return '/' . $path . $avatar;
            }
        }

         return self::getAvatarDefault();
    }

    /*
	 * author: HoanDV
	 * time ago
	 * input: $time_ago = datetime
     * DES:
	 */
    public static function timeAgo($time_ago)
    {
        $cur_time 	= time();
        $time_elapsed 	= $cur_time - $time_ago;
        $seconds 	= $time_elapsed ;
        $minutes 	= round($time_elapsed / 60 );
        $hours 		= round($time_elapsed / 3600);
        $days 		= round($time_elapsed / 86400 );
        $weeks 		= round($time_elapsed / 604800);
        $months 	= round($time_elapsed / 2600640 );
        $years 		= round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            echo "$seconds giây trước";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                echo "một phúc trước";
            }
            else{
                echo "$minutes phút trước";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                echo "một giờ trước";
            }else{
                echo "$hours giờ trước";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                echo "một ngày trước";
            }else{
                echo "$days ngày trước";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                echo "một tuần trước";
            }else{
                echo "$weeks tuần trước";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                echo "một tháng trước";
            }else{
                echo "$months tháng trước";
            }
        }
        //Years
        else{
            if($years==1){
                echo "một năm trước";
            }else{
                echo "$years năm trước";
            }
        }
    }
    
}
