<?php

namespace DVGroup\Users;

use Zend\Session\Container;
use DVGroup\Operation\ServiceLocatorFactory;

class AuthUser {

    private $session;
    private $tables;

    public function __construct() {
        $this->tables = array();
        $this->session = new Container("USER");
    }

    public function setLoggedIn($user_id) {
        $sessionManager = new \Zend\Session\SessionManager ();
        $ttl = 60 * 60 * 24 * 30;
        $sessionManager->rememberMe($ttl);
        $session = new \Zend\Session\Container('USER');
        $session->setExpirationSeconds($ttl);
        $session->user_id = $user_id;
    }

    public function isLoggedIn() {
        if ($this->session->offsetExists('user_id')) {
            return true;
        }
        return false;
    }
    
    public function getUser(){
        $sm = ServiceLocatorFactory::getInstance();
        $user_obj = $sm->get('UserTable');
        if($this->session->offsetExists('user_id')){
            $user = $user_obj->getById($this->session->user_id);
            return $user;
        }
        return NULL;
    }
}
