-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 03, 2014 at 05:44 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `titmu`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text COLLATE utf8_unicode_ci,
  `is_parent` int(11) DEFAULT '0' COMMENT '0: no, 1: yes',
  `parent_id` int(11) DEFAULT NULL,
  `rank_order` int(11) DEFAULT '0',
  `slug` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `is_parent`, `parent_id`, `rank_order`, `slug`, `description`) VALUES
(1, 'Menu item 1', 1, NULL, 0, 'trang-tri-nha-cua', NULL),
(2, 'Menu item 1', 1, NULL, 0, 'san-pham-danh-cho-con-trai', 'Tạo cá tính riêng cho bạn với quần áo handmade, đồ chỉ dành cho con trai và nhiều hơn thế nữa.'),
(3, 'Menu item 1', 1, NULL, 0, 'san-pham-danh-cho-con-gai', NULL),
(4, 'Menu item 1', 1, NULL, 0, 'san-pham-danh-cho-tre-em', NULL),
(5, 'Menu item 1', 1, NULL, 0, 'do-dung-co-dien', NULL),
(6, 'Menu item 1', 0, 2, 2, 'tui-vi-da-nam', ''),
(7, 'Menu item 1', 0, 2, 1, 'ao-so-mi-nam', '1'),
(8, 'Menu item 1', 0, 2, 0, 'ao-phong-nam', '0'),
(9, 'Menu item 1', 0, 2, 3, 'quan-nam', NULL),
(10, 'Menu item 1', 0, 2, 0, 'giay-dep-nam-vintage-handmade', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_uploaded`
--

CREATE TABLE IF NOT EXISTS `image_uploaded` (
  `image_uploaded_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `image_link` text COLLATE utf8_unicode_ci,
  `image_thumbnail` text COLLATE utf8_unicode_ci,
  `image_width` int(11) DEFAULT NULL,
  `image_height` int(11) DEFAULT NULL,
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_uploaded_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `image_uploaded`
--

INSERT INTO `image_uploaded` (`image_uploaded_id`, `user_id`, `image_link`, `image_thumbnail`, `image_width`, `image_height`, `since`) VALUES
(1, 1, 'http://test.com/media/upload/2014/10/16/8ef206b7c2dd271a9764518e1a1c16f4.jpg', 'http://test.com/media/upload/thumbnail/2014/10/16/8ef206b7c2dd271a9764518e1a1c16f4.jpg', 550, 550, '2014-10-16 09:27:51'),
(2, 1, 'http://test.com/media/upload/2014/10/16/52b0f21e293c5526f9a1ee5f06f87845.jpg', 'http://test.com/media/upload/thumbnail/2014/10/16/52b0f21e293c5526f9a1ee5f06f87845.jpg', 500, 360, '2014-10-16 09:28:40'),
(3, 1, 'http://test.com/media/upload/2014/10/16/bc39eb9944b028cf5927acefd161c0cf.jpg', 'http://test.com/media/upload/thumbnail/2014/10/16/bc39eb9944b028cf5927acefd161c0cf.jpg', 398, 600, '2014-10-16 09:29:37'),
(4, 2, 'http://test.com/media/upload/2014/10/16/82c49f778c5528715cddd2346bee5d40.jpg', 'http://test.com/media/upload/thumbnail/2014/10/16/82c49f778c5528715cddd2346bee5d40.jpg', 570, 570, '2014-10-16 09:31:08'),
(5, 1, 'http://test.com/media/upload/2014/10/18/019bd54719dc78867f2fe42cc78ce922.jpg', 'http://test.com/media/upload/thumbnail/2014/10/18/019bd54719dc78867f2fe42cc78ce922.jpg', 566, 600, '2014-10-18 02:47:09'),
(6, 1, 'http://test.com/media/upload/2014/11/03/490b4a19ddcd44c01146db3e192c2042.jpg', 'http://test.com/media/upload/thumbnail/2014/11/03/490b4a19ddcd44c01146db3e192c2042.jpg', 550, 550, '2014-11-03 09:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `titmu_shop_id` int(11) DEFAULT NULL,
  `product_name` text COLLATE utf8_unicode_ci,
  `product_owner` int(11) DEFAULT NULL,
  `product_price` float DEFAULT NULL,
  `product_desc` text COLLATE utf8_unicode_ci,
  `featured_image` int(11) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `titmu_shop_id`, `product_name`, `product_owner`, `product_price`, `product_desc`, `featured_image`, `date_created`, `last_update`) VALUES
(1, 6, 'Giày búp bê', 1, 99000, 'Mô tả về sản phẩm', 6, '2014-11-02 21:20:51', '2014-11-03 09:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE IF NOT EXISTS `product_tag` (
  `product_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product_user_like`
--

CREATE TABLE IF NOT EXISTS `product_user_like` (
  `product_user_like_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `user_collection_id` int(11) DEFAULT NULL,
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_user_like_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `titmu_contact_type`
--

CREATE TABLE IF NOT EXISTS `titmu_contact_type` (
  `titmu_contact_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_type_name` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) NOT NULL,
  `rank_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`titmu_contact_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `titmu_contact_type`
--

INSERT INTO `titmu_contact_type` (`titmu_contact_type_id`, `contact_type_name`, `parent_id`, `rank_order`) VALUES
(1, 'Skype', 0, 3),
(2, 'Yahoo', 0, 4),
(3, 'Whatsapp', 7, 3),
(4, 'Viber', 7, 2),
(5, 'Zalo', 7, 1),
(6, 'Facebook', 0, 2),
(7, 'Phone', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `titmu_shop`
--

CREATE TABLE IF NOT EXISTS `titmu_shop` (
  `titmu_shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `shop_name` text COLLATE utf8_unicode_ci,
  `shop_desc` text COLLATE utf8_unicode_ci,
  `shop_address` text COLLATE utf8_unicode_ci,
  `shop_contact` text COLLATE utf8_unicode_ci,
  `shop_url` text COLLATE utf8_unicode_ci,
  `open_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT '1' COMMENT '0: no, 1: yes',
  `shop_avatar` text COLLATE utf8_unicode_ci,
  `shop_featured_image` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`titmu_shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `titmu_shop`
--

INSERT INTO `titmu_shop` (`titmu_shop_id`, `user_id`, `shop_name`, `shop_desc`, `shop_address`, `shop_contact`, `shop_url`, `open_since`, `is_active`, `shop_avatar`, `shop_featured_image`) VALUES
(6, 1, 'Tên Cửa hàng', 'Tất cả vì khách hàng thân thương, shop luôn tận tình mang đến những sản phẩm tốt nhất', '38 Bà Triệu, Hai Bà Trưng, Hà Nội', '0987686868', 'kiennguyen07', '2014-10-14 10:11:06', 1, 'img/shop/default_avatar.jpg', 'img/shop/shop_default.jpg'),
(7, 2, 'Tên Cửa hàng', 'Tất cả vì khách hàng thân thương, shop luôn tận tình mang đến những sản phẩm tốt nhất', '38 Bà Triệu, Hai Bà Trưng, Hà Nội', '0987686868', 'vicent.ng.07', '2014-10-16 08:53:03', 1, 'img/shop/default_avatar.jpg', 'img/shop/shop_default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `titmu_shop_contact`
--

CREATE TABLE IF NOT EXISTS `titmu_shop_contact` (
  `titmu_shop_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `titmu_shop_id` int(11) DEFAULT NULL,
  `titmy_contact_type_id` int(11) DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`titmu_shop_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `username` text COLLATE utf8_unicode_ci,
  `fullname` text COLLATE utf8_unicode_ci,
  `gender` int(11) DEFAULT NULL COMMENT '0: female, 1: male',
  `shop_url` text COLLATE utf8_unicode_ci,
  `is_active` int(11) DEFAULT '1',
  `since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `json_data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `username`, `fullname`, `gender`, `shop_url`, `is_active`, `since`, `json_data`) VALUES
(1, 'kiennguyen.hut@gmail.com', 'kiennguyen07', 'Nguyen Trung Kien', 1, 'kiennguyen07', 1, '2014-10-14 06:41:24', '{"id":"821235461259908","email":"kiennguyen.hut@gmail.com","favorite_athletes":[{"id":"219643781416878","name":"Rio Ferdinand"},{"id":"245323855526590","name":"Wayne Rooney"},{"id":"315972331771291","name":"M\\u1ea1c H\\u1ed3ng Qu\\u00e2n"}],"favorite_teams":[{"id":"7724542745","name":"Manchester United"}],"first_name":"Nguyen","gender":"male","last_name":"Kien","link":"https:\\/\\/www.facebook.com\\/kiennguyen07","locale":"en_US","middle_name":"Trung","name":"Nguyen Trung Kien","timezone":7,"updated_time":"2014-08-23T19:19:18+0000","username":"kiennguyen07","verified":true}'),
(2, 'kiennt@live.com', 'vicent.ng.07', 'Vicent Ng', 1, NULL, 1, '2014-10-16 08:52:57', '{"id":"100001709397386","birthday":"12\\/02\\/1989","email":"kiennt@live.com","favorite_teams":[{"id":"100153050163519","name":"Fan Girl Arsenal"}],"first_name":"Vicent","gender":"male","languages":[{"id":"104059856296458","name":"Vietnamese"},{"id":"106059522759137","name":"English"},{"id":"108224912538348","name":"French"}],"last_name":"Ng","link":"https:\\/\\/www.facebook.com\\/vicent.ng.07","location":{"id":"106388046062960","name":"Hanoi, Vietnam"},"locale":"en_US","name":"Vicent Ng","timezone":7,"updated_time":"2014-09-03T15:53:48+0000","username":"vicent.ng.07","verified":true}');

-- --------------------------------------------------------

--
-- Table structure for table `user_collection`
--

CREATE TABLE IF NOT EXISTS `user_collection` (
  `user_collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `collection_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_default` int(11) DEFAULT '0',
  `since` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_collection_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user_collection`
--

INSERT INTO `user_collection` (`user_collection_id`, `user_id`, `collection_name`, `is_default`, `since`) VALUES
(2, 1, 'Đang muốn mua đây', 1, '2014-10-17 06:57:21'),
(3, 1, 'Từ từ rồi sẽ mua', 0, '2014-10-17 06:57:21'),
(4, 1, 'Lưu lại xem từ từ', 0, '2014-10-17 06:57:21');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
