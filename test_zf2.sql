-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 14, 2014 at 05:44 PM
-- Server version: 5.5.38-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_zf2`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` text COLLATE utf8_unicode_ci,
  `is_parent` int(11) DEFAULT '0' COMMENT '0: no, 1: yes',
  `parent_id` int(11) DEFAULT NULL,
  `rank_order` int(11) DEFAULT '0',
  `slug` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `is_parent`, `parent_id`, `rank_order`, `slug`, `description`) VALUES
(1, 'Menu item 1', 1, NULL, 0, 'trang-tri-nha-cua', NULL),
(2, 'Menu item 1', 1, NULL, 0, 'san-pham-danh-cho-con-trai', 'Tạo cá tính riêng cho bạn với quần áo handmade, đồ chỉ dành cho con trai và nhiều hơn thế nữa.'),
(3, 'Menu item 1', 1, NULL, 0, 'san-pham-danh-cho-con-gai', NULL),
(4, 'Menu item 1', 1, NULL, 0, 'san-pham-danh-cho-tre-em', NULL),
(5, 'Menu item 1', 1, NULL, 0, 'do-dung-co-dien', NULL),
(6, 'Menu item 1', 0, 2, 2, 'tui-vi-da-nam', ''),
(7, 'Menu item 1', 0, 2, 1, 'ao-so-mi-nam', '1'),
(8, 'Menu item 1', 0, 2, 0, 'ao-phong-nam', '0'),
(9, 'Menu item 1', 0, 2, 3, 'quan-nam', NULL),
(10, 'Menu item 1', 0, 2, 0, 'giay-dep-nam-vintage-handmade', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `titmu_shop_id` int(11) DEFAULT NULL,
  `product_name` text COLLATE utf8_unicode_ci,
  `product_owner` int(11) DEFAULT NULL,
  `featured_image` text COLLATE utf8_unicode_ci,
  `date_created` timestamp NULL DEFAULT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `titmu_shop_id`, `product_name`, `product_owner`, `featured_image`, `date_created`, `last_update`) VALUES
(1, NULL, 'Always', 1, 'http://m.topring.vn/upload/img/Bon%20Jovi%201.jpg', NULL, '2014-10-13 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `product_tag`
--

CREATE TABLE IF NOT EXISTS `product_tag` (
  `product_tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_tag_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `product_tag`
--

INSERT INTO `product_tag` (`product_tag_id`, `product_id`, `category_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `titmu_contact_type`
--

CREATE TABLE IF NOT EXISTS `titmu_contact_type` (
  `titmu_contact_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_type_name` text COLLATE utf8_unicode_ci,
  `parent_id` int(11) NOT NULL,
  `rank_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`titmu_contact_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `titmu_contact_type`
--

INSERT INTO `titmu_contact_type` (`titmu_contact_type_id`, `contact_type_name`, `parent_id`, `rank_order`) VALUES
(1, 'Skype', 0, 3),
(2, 'Yahoo', 0, 4),
(3, 'Whatsapp', 7, 3),
(4, 'Viber', 7, 2),
(5, 'Zalo', 7, 1),
(6, 'Facebook', 0, 2),
(7, 'Phone', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `titmu_shop`
--

CREATE TABLE IF NOT EXISTS `titmu_shop` (
  `titmu_shop_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `shop_name` text COLLATE utf8_unicode_ci,
  `shop_desc` text COLLATE utf8_unicode_ci,
  `shop_address` text COLLATE utf8_unicode_ci,
  `shop_contact` text COLLATE utf8_unicode_ci,
  `shop_url` text COLLATE utf8_unicode_ci,
  `open_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT '1' COMMENT '0: no, 1: yes',
  `shop_avatar` text COLLATE utf8_unicode_ci,
  `shop_featured_image` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`titmu_shop_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `titmu_shop`
--

INSERT INTO `titmu_shop` (`titmu_shop_id`, `user_id`, `shop_name`, `shop_desc`, `shop_address`, `shop_contact`, `shop_url`, `open_since`, `is_active`, `shop_avatar`, `shop_featured_image`) VALUES
(6, 1, 'Tên Cửa hàng', 'Tất cả vì khách hàng thân thương, shop luôn tận tình mang đến những sản phẩm tốt nhất', '38 Bà Triệu, Hai Bà Trưng, Hà Nội', '0987686868', 'kiennguyen07', '2014-10-14 10:11:06', 1, 'img/shop/default_avatar.jpg', 'img/shop/shop_default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `titmu_shop_contact`
--

CREATE TABLE IF NOT EXISTS `titmu_shop_contact` (
  `titmu_shop_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `titmu_shop_id` int(11) DEFAULT NULL,
  `titmy_contact_type_id` int(11) DEFAULT NULL,
  `value` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`titmu_shop_contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text COLLATE utf8_unicode_ci,
  `facebook` text COLLATE utf8_unicode_ci,
  `fullname` text COLLATE utf8_unicode_ci,
  `gender` int(11) DEFAULT NULL COMMENT '0: female, 1: male',
  `shop_url` text COLLATE utf8_unicode_ci,
  `is_active` int(11) DEFAULT '1',
  `since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `json_data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `facebook`, `fullname`, `gender`, `shop_url`, `is_active`, `since`, `json_data`) VALUES
(1, 'kiennguyen.hut@gmail.com', 'kiennguyen07', 'Nguyen Trung Kien', 1, 'kiennguyen07', 1, '2014-10-14 06:41:24', '{"id":"821235461259908","email":"kiennguyen.hut@gmail.com","favorite_athletes":[{"id":"219643781416878","name":"Rio Ferdinand"},{"id":"245323855526590","name":"Wayne Rooney"},{"id":"315972331771291","name":"M\\u1ea1c H\\u1ed3ng Qu\\u00e2n"}],"favorite_teams":[{"id":"7724542745","name":"Manchester United"}],"first_name":"Nguyen","gender":"male","last_name":"Kien","link":"https:\\/\\/www.facebook.com\\/kiennguyen07","locale":"en_US","middle_name":"Trung","name":"Nguyen Trung Kien","timezone":7,"updated_time":"2014-08-23T19:19:18+0000","username":"kiennguyen07","verified":true}');

-- --------------------------------------------------------

--
-- Table structure for table `user_cookie`
--

CREATE TABLE IF NOT EXISTS `user_cookie` (
  `user_cookie_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `cookie` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user_cookie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_cookie`
--

INSERT INTO `user_cookie` (`user_cookie_id`, `user_id`, `cookie`) VALUES
(1, 1, '72f5350e17f17181e20b4339738a05a5');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
