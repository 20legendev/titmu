<?php
namespace Shop\Model;

use Zend\Db\TableGateway\TableGateway;

class ShopTable{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}
	
	public function getByUserId($user_id){
		$result = $this->tableGateway->select(array('user_id'=>$user_id));
		$row = $result->current();
		if($row){
			return $row;
		}
		return null;
	}
	
	public function addNew($arr){
		return $this->tableGateway->insert($arr);
	}
	
	public function checkShopProduct($shop_id, $product_id){
		$result = $this->tableGateway->select(array(
			'titmu_shop_id'=>$shop_id,
			'product_id'=>$product_id
		));
		$row = $result->current();
		if($row) return $row;
		return NULL;
	}
	
	public function checkShopName($shop_url){
		$result = $this->tableGateway->select(array('shop_url'=>$shop_url));
		if($result->count() > 0){
			return true;
		}
		return false;
	}
	
}
