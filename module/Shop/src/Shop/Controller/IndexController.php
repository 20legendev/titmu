<?php

namespace Shop\Controller;

use DVGroup\Operation\BaseController;
use Product\Form\EditProductForm;
use Zend\View\Model\ViewModel;

class IndexController extends BaseController {

    public function indexAction() {
        if (!$this->isLoggedIn()) {
            return $this->redirect()->toRoute('loginshop');
        }
        $user = $this->getUser();
        $shop = $this->getTable('Shop\Model\ShopTable');
        $my_shop = $shop->getByUserId($user['user_id']);
        if (!$my_shop) {
            return $this->redirect()->toRoute('shop.openshop');
        }

        $product = $this->getTable('Product\Model\ProductTable');
        $product_data = $product->getByShop($my_shop->titmu_shop_id, 0);
        $view = new ViewModel(array(
            'shop' => $my_shop,
            'product' => $product_data
        ));

        $form = new EditProductForm('edit');
        $view->setVariable('edit', $form);
        return $view;
    }

    public function loginAction() {
        return new ViewModel();
    }

    public function openShopAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            if (!$this->user) {
                $this->setResponse(array(
                    'status' => 1,
                    'msg' => 'NOT_LOG_IN'
                ));
                return $this->response;
            }


            $shop = $this->getTable('Shop\Model\ShopTable');
            $shop_data = $shop->getByUserId($this->user->user_id);
            if (!$shop_data) {
                $config = $this->getServiceLocator()->get('config');
                $config = $config['shop'];

                $shop_url = $this->getShopName($this->user->username);
                $shop->addNew(array(
                    'user_id' => $this->user->user_id,
                    'shop_url' => $shop_url,
                    'shop_featured_image' => $config['default_featured_image'],
                    'shop_avatar' => $config['default_avatar'],
                    'shop_name' => $config['default_name'],
                    'shop_desc' => $config['default_desc'],
                    'shop_address' => $config['default_address'],
                    'shop_contact' => $config['default_contact'],
                ));
            }
            return $this->redirect()->toRoute('shop.myshop');
        }
        return new ViewModel();
    }

    private function getShopName($username) {
        $found = 0;
        $count = 1;
        $user = $this->getTable('Shop\Model\ShopTable');
        $shop_url = $username;
        while (!$found) {
            if (!$user->checkShopName($shop_url)) {
                $found = 1;
                break;
            } else {
                $shop_url .= $count;
            }
        }
        return $shop_url;
    }

}
