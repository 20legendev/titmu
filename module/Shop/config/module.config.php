<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Shop\Controller\Index' => 'Shop\Controller\IndexController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
            'shop.myshop' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/cua-hang',
                    'constraints' => array(
                    ),
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            
			'loginshop' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/shop/dang-nhap',
                    'constraints' => array(
                    ),
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Index',
                        'action'     => 'login',
                    ),
                ),
            ),
            'shop.openshop' => array(
                'type'    => 'literal',
                'options' => array(
                    'route'    => '/shop/tao-cua-hang',
                    'constraints' => array(
                    ),
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Index',
                        'action'     => 'open-shop',
                    ),
                ),
            ),
        ),
    ),
	
    'view_manager' => array(
        'template_path_stack' => array(
            'shop' => __DIR__ . '/../view',
        ),
    ),
);
