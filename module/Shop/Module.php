<?php
namespace Shop;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Shop\Model\Shop;
use Shop\Model\ShopTable;

class Module {

	public function getAutoloaderConfig() {
		return array('Zend\Loader\ClassMapAutoloader' => array(__DIR__ . '/autoload_classmap.php', ), 'Zend\Loader\StandardAutoloader' => array('namespaces' => array(__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__, ), ), );
	}

	public function getConfig() {
		return
		include __DIR__ . '/config/module.config.php';
	}

	public function getServiceConfig() {
		return array('factories' => 
		array('Shop\Model\ShopTable' => function($sm) {
			$tableGateway = $sm -> get('ShopTableGateway');
			$table = new ShopTable($tableGateway);
			return $table;
		}, 'ShopTableGateway' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			$resultSetPrototype = new ResultSet();
			$resultSetPrototype -> setArrayObjectPrototype(new Shop());
			return new TableGateway('titmu_shop', $dbAdapter, null, $resultSetPrototype);
		}, ), );
	}

}
