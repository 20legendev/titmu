<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Home;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Home\Model\CategoryTable;
use Home\Model\ArtistAlbum;
use Home\Model\ArtistAlbumTable;
use Home\Model\ImageUpload;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use DVGroup\Operation\ServiceLocatorFactory;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $app = $e->getApplication();
        $eventManager = $app->getEventManager();
        $moduleRouteListener = new ModuleRouteListener ();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_DISPATCH, [
            $this, 'manage'], 100);
        setlocale(LC_MONETARY, 'en_US');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        ServiceLocatorFactory::setInstance($e->getApplication()->getServiceManager());
    }

    public function setLayoutTitle($e) {
        $config = $e->getApplication()->getServiceManager()->get('config');
        $vm = $e->getApplication()->getServiceManager()->get('viewHelperManager');
        $headTitleHelper = $vm->get('headTitle');
        $headTitleHelper->set($config['seo']['default']);
    }

    public function manage(MvcEvent $e) {
        $this->setLayoutTitle($e);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'DVGroup' => __DIR__ . '/../../vendor/DVGroup'
                )
            )
        );
    }

    public function getViewHelperConfig() {
        return ['factories' => [
                'getHeader' => function ($sm) {
                    $serviceLocator = $sm->getServiceLocator();
                    return new Widget\HeaderWidget($serviceLocator);
                },
                'getMenu' => function ($sm) {
                    $serviceLocator = $sm->getServiceLocator();
                    return new Widget\MenuWidget($serviceLocator);
                }
            ]
        ];
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'CategoryTable' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new CategoryTable(new TableGateway('category', $dbAdapter));
                    return $table;
                },
                'Home\Model\ImageUpload' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new ImageUpload(new TableGateway('image_uploaded', $dbAdapter));
                    return $table;
                }
            )
        );
    }

}
