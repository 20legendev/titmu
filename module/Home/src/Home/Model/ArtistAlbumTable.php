<?php
namespace Home\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ArtistAlbumTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}

	public function fetchAll() {
		$resultSet = $this -> tableGateway -> select();
		return $resultSet;
	}

	public function getNew() {
		$data = $this->tableGateway->select(function (Select $select) {
		    $select->where('1 = 1');	
		    $select->order('lastUpdate DESC');
			$select->limit(25);
			$select->offset(0);
		});
		return $data;
	}

	public function saveAlbum(Album $album) {
		$data = array('artist' => $album -> artist, 'title' => $album -> title, );

		$id = (int)$album -> id;
		if ($id == 0) {
			$this -> tableGateway -> insert($data);
		} else {
			if ($this -> getAlbum($id)) {
				$this -> tableGateway -> update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteAlbum($id) {
		$this -> tableGateway -> delete(array('id' => $id));
	}

}
