<?php

namespace Home\Model;

use Zend\Db\Sql\Select;

class CategoryTable extends \DVGroup\Db\Model\BaseTable {

    public function getTopLevel() {
        $data = $this->tableGateway->select(function (Select $select) {
            $select->where(array('is_parent' => 1, 'parent_id' => NULL));
            $select->order('rank_order ASC');
        });
        return $data;
    }

    public function getBySlug($slug) {
        $rowset = $this->tableGateway->select(array('slug' => $slug));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getSubCategory($category_id) {
        $select = new Select();
        $select->from($this->tableGateway->getTable());
        $select->where(array('parent_id' => $category_id));
        $select->order('rank_order ASC');
        $data = $this->tableGateway->selectWith($select);
        return $data;
    }

    public function getAlbum($id) {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveAlbum(Album $album) {
        $data = array('artist' => $album->artist, 'title' => $album->title,);

        $id = (int) $album->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getAlbum($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteAlbum($id) {
        $this->tableGateway->delete(array('id' => $id));
    }

}
