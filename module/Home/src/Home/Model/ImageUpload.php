<?php
namespace Home\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\TableGateway\TableGateway;

class ImageUpload implements InputFilterAwareInterface{
		
	public $image_uploaded_id;
	public $user_id;
	public $image_link;
	public $image_thumbnail;
	public $since;
    
	public function __construct(TableGateway $tableGateway){
		$this -> tableGateway = $tableGateway;
	}
	
	public function addNew($array){
		$this->prepareData($array);
		$row = $this->checkUserLink($array['user_id'],$array['image_link']); 
		if($row === NULL){
			return $this->tableGateway->insert($array);
		}else{
			return $this->tableGateway->update($array, array('image_uploaded_id'=>$row->image_uploaded_id));
		}	
	}
	
	public function doUpdate($array, $image_uploaded_id){
		$this->prepareData($array);
		return $this->tableGateway->update($array, array('image_uploaded_id'=>$image_uploaded_id));
	}
	
	public function checkUserLink($data){
		$result = $this->tableGateway->select(array('user_id'=>$data['user_id'], 'image_link'=>$data['image_link']));
		$row = $result->current();
		if($row){
			return $row;
		}
		return NULL;
	}
	
	
	public function exchangeArray($data) {
		foreach($data as $key=>$value){
			$this->$key = (isset($data[$key])) ? $value : null;
		}
	}
	
	public function getLastInsertId(){
		return $this->tableGateway->lastInsertValue;
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
	
	private function prepareData(&$data){
		if($data && $data['image_link']){
			$size = getimagesize($data['image_link']);
			$data['image_width'] =  $size[0];
			$data['image_height'] =  $size[1];
		}
		return $data;
	}

}
