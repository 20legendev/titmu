<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Home\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class AlbumController extends BaseController {
	
    public function indexAction() {
		$category = $this->getCategoryTable()->getTopMenu();
		$this->_viewModel->setVariable('category', $category);
		$this->_viewModel->setVariable('slug', $this->params('slug'));
		
		$cat = $this->getTable('Home\Model\CategoryTable');
		$catObj = $cat->getBySlug($this->params('slug'));
		
		$music = $this->getTable('Home\Model\MusicTable');
		$this->_viewModel->setVariable('music', $music->getByCategory($catObj->id));
		
		return $this->_viewModel;
    }
}
