<?php

namespace Home\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;

class IndexController extends BaseController {

    public function indexAction() {
        $viewModel = new ViewModel();
        $product = $this->getTable('Product\Model\ProductTable');
        $viewModel->setVariable('product', $product->getByPage());
        return $viewModel;
    }

}
