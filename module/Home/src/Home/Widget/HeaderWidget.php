<?php

namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class HeaderWidget extends AbstractHelper {

    protected $service;

    public function __construct(ServiceManager $service) {
        $this->service = $service;
    }

    protected function getTable($table_name) {
        return $this->service->get($table_name);
    }

    public function __invoke() {
        return $this;
    }

    public function render() {
        $menu = $this->getTable('CategoryTable')->getTopLevel();
        $auth = new \DVGroup\Users\AuthUser();
        if ($auth->isLoggedIn()) {
            $user = $auth->getUser();
        }
        return $this->getView()->render('home/widget/header', [
                    'menu' => $menu,
                    'user' => $user
        ]);
    }

}
