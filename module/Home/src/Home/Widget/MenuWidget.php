<?php

namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;
use DVGroup\Auth\AuthUser;

class MenuWidget extends AbstractHelper {

    protected $service;

    public function __construct(ServiceManager $service) {
        $this->service = $service;
    }

    protected function getTable($table_name) {
        return $this->service->get($table_name);
    }

    public function __invoke() {
        return $this;
    }

    public function userMenu() {
        /*
        $auth = new AuthUser();
        if ($auth->isAuthen()) {
            $user = $auth->getUser();
        };
        $event = $this->getTable('Artist\Model\TbEvent');
        $event_join = $this->getTable('Artist\Model\TbEventAttendance');
        $event_tag = $this->getTable('Artist\Model\TbEventTag');
        $data = $event->getUpComing();
        $arr = array();
        $tmp;
        foreach ($data as $identify) {
            $tmp = $event->getEventByIdentify($identify);
            $tmp ['attended'] = isset($user) ? $event_join->isAttended($user ['username'], $identify) : 0;
            $a = $event_tag->getItemTag($identify);
            if (count($a) > 0) {
                $tmp ['tag_name'] = $a[0];
            }
            $arr[] = $tmp;
        }
         */
        return $this->getView()->render('home/widget/user-menu', array());
    }

}

?>