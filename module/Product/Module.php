<?php
namespace Product;

use Product\Model\Product;
use Product\Model\ProductTable;
use Product\Model\Shop;
use Product\Model\ShopTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module {

	public function getAutoloaderConfig() {
		return array('Zend\Loader\ClassMapAutoloader' => array(__DIR__ . '/autoload_classmap.php', ), 'Zend\Loader\StandardAutoloader' => array('namespaces' => array(__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__, ), ), );
	}

	public function getConfig() {
		return
		include __DIR__ . '/config/module.config.php';
	}

	public function getServiceConfig() {
		return array('factories' => array(
		'Product\Model\ProductTable' => function($sm) {
			$tableGateway = $sm -> get('ProductTableGateway');
			$table = new ProductTable($tableGateway);
			return $table;
		}, 'ProductTableGateway' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			$resultSetPrototype = new ResultSet();
			$resultSetPrototype -> setArrayObjectPrototype(new Product());
			return new TableGateway('product', $dbAdapter, null, $resultSetPrototype);
		},'Product\Model\ShopTable' => function($sm) {
			$tableGateway = $sm -> get('ShopTableGateway');
			$table = new ShopTable($tableGateway);
			return $table;
		}, 'ShopTableGateway' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			$resultSetPrototype = new HydratingResultSet();
			$resultSetPrototype -> setObjectPrototype(new Shop());
			return new TableGateway('titmu_shop', $dbAdapter, null, $resultSetPrototype);
		}), );
	}

}
