<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Product\Controller\Index' => 'Product\Controller\IndexController',
            'Product\Controller\Service' => 'Product\Controller\ServiceController',
            'Product\Controller\Shop' => 'Product\Controller\ShopController',
        ),
    ),
    
    'router' => array(
        'routes' => array(
            
            'product' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/danh-muc[/:slug].html',
                    'constraints' => array(
                        'slug' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Product\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            
            'web-service' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/pws[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                    	'__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Service',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
	
    'view_manager' => array(
        'template_path_stack' => array(
            'product' => __DIR__ . '/../view',
        ),
        'template_map' => array(
            'template/product'           => __DIR__ . '/../view/partial/product.phtml',
        ),
    ),
);
