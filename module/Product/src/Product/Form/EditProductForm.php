<?php
namespace Product\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\Form\Element\Number;
use Zend\Form\Element\Text;
use Zend\Form\Element\Textarea;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Hidden;

class EditProductForm extends Form
{
    public function __construct($name = null){
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');
		$this->addElement();
    }
	
	public function addElement(){
		$product_id = new Hidden('product_id');
        $product_id->setValue(0);
			
		$product_name = new Text('product_name');
        $product_name->setAttributes(array(
        			'class'=>'product-name editable',
                 	'placeholder'=>"Tên sản phẩm"
				));
				 
		$product_desc = new Textarea('product_desc');
		$product_desc->setValue("Mô tả sản phẩm")
			->setAttributes(array(
				'class'=>'product-desc editable'
			));
		
		$product_price = new Text('product_price');
		$product_price->setAttributes(array(
				'class'=>'title'
			));
		
        $product_price->setLabel("Giá bán")
        	->setAttributes(array(
				'placeholder'=>99000
			));
			
		$publish = new Submit('publish');
		$publish->setValue('Đăng sản phẩm')
			->setAttributes(
				array(
					'class'=>'save-button green-button'
				)
			);
		$save = new Submit('save');
		$save->setValue('Lưu lại')
			->setAttributes(
				array(
					'class'=>'green-button blue'
				)
			);
				 
		$this->add($product_name);		
		$this->add($product_desc);
		$this->add($product_price);
		$this->add($product_id);
		$this->add($publish);
		$this->add($save);
	}
}
