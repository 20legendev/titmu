<?php
namespace Product\Form;

use Zend\Form\Form;

class OpenShopForm extends Form
{
    public function __construct($name = null){
        // we want to ignore the name passed
        parent::__construct('album');
        $this->setAttribute('method', 'post');
    }
}
