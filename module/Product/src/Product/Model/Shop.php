<?php
namespace Product\Model;

use Zend\InputFilter\Factory as InputFactory;     // <-- Add this import
use Zend\InputFilter\InputFilter;                 // <-- Add this import
use Zend\InputFilter\InputFilterAwareInterface;   // <-- Add this import
use Zend\InputFilter\InputFilterInterface;        // <-- Add this import


class Shop implements InputFilterAwareInterface{
	public $titmu_shop_id;
	public $user_id;
	public $shop_name;
	public $shop_desc;
	public $shop_address;
	public $shop_contact;
	public $shop_url;
	public $open_since;
	public $is_active;
	public $shop_avatar;
	public $shop_featured_image;
	protected $inputFilter;
    
	public function exchangeArray($data) {
		foreach($data as $key=>$value){
			$this->$key = (isset($data[$key])) ? $value : null;
		}
	}
	
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
	

}
