<?php
namespace Product\Model;

use Zend\Db\TableGateway\TableGateway;

class ShopTable{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}
	
	public function getByUserId($user_id){
		$result = $this->tableGateway->select(array('user_id'=>$user_id));
		$row = $result->current();
		if($row){
			return $row;
		}
		return null;
	}
	
}
