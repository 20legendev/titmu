<?php
namespace Product\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ProductTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}
	
	public function addNew($arr){
		return $this->tableGateway->insert($arr);
	}

	public function getByCategory($category, $page) {
		$page = intval($page);
		$select = new Select();
		$select->from(array('p'=>$this->tableGateway->getTable()));
		$select->join(array('t'=>'product_tag'), 't.product_id = p.product_id');
		$select->where(array('t.category_id'=>$category));
		$select->limit(20);
		$select->offset($page * 20);
		$resultSet = $this -> tableGateway -> selectWith($select);
		return $this->toArray($resultSet);
	}
	
	public function getByPage($page = 0){
		$select = new Select();
		$select->from(array('p' => 'product'));
		$select->join(array('i' => 'image_uploaded'), 'image_uploaded_id = p.featured_image');
		$select->join(array('s' => 'titmu_shop'), 's.titmu_shop_id = p.titmu_shop_id');
		
		$select->limit(25);
		$select->order('p.last_update DESC');
		$select->offset($page * 25);
		
		$result = $this -> tableGateway -> selectWith($select); 
		$result->buffer();
		
		return $result;
	}

	public function getByShop($shop_id, $page) {
		$select = new Select();
		$select->from(array('p' => 'product'));
		$select->join(array('i' => 'image_uploaded'), 'image_uploaded_id = p.featured_image');
		$select->join(array('s' => 'titmu_shop'), 's.titmu_shop_id = p.titmu_shop_id');
		$select->where(array('p.titmu_shop_id' => $shop_id));
		
		$select->limit(25);
		$select->order('p.last_update DESC');
		$select->offset($page * 25);
		
		$result = $this -> tableGateway -> selectWith($select); 
		$result->buffer();
		
		return $result;
	}

	public function checkShopProduct($shop_id, $product_id){
		$result = $this->tableGateway->select(array('titmu_shop_id'=>$shop_id, 'product_id'=>$product_id));
		$row = $result->current();
		if($row){
			return true;
		}
		return false;
	}

	public function getByProductId($product_id) {
		$result = $this->tableGateway->select(array('product_id'=>$product_id));
		$row = $result->current();
		if($row){
			return $row;
		}
		return NULL;
	}

	public function update($array, $product_id){
		return $this->tableGateway->update($array, array('product_id'=>$product_id));
	}

	public function toArray($data){
		return \Zend\Stdlib\ArrayUtils::iteratorToArray($data);
	}

}
