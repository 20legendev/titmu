<?php

namespace Product\Controller;

use DVGroup\Operation\BaseController;
use Zend\View\Model\ViewModel;

class IndexController extends BaseController {

    public function indexAction() {
        $slug = $this->params()->fromRoute('slug', "");
        $view = new ViewModel();
        if ($slug != "") {
            $catObj = $this->getTable('CategoryTable');
            $cur_cate = $catObj->getBySlug($slug);
            $data = $catObj->getSubCategory($cur_cate->category_id);
            $view->cur_category = $cur_cate;
            $view->sub_category = $data;
        }
        return $view;
    }

}
