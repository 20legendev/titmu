<?php

namespace Product\Controller;

use DVGroup\Operation\BaseController;
use Product\Form\EditProductForm;

class ServiceController extends BaseController {

    public function indexAction() {
        $this->setResponse(array());
        return $this->response;
    }

    public function getProductAction() {
        $product = $this->getTable('Product\Model\ProductTable');
        $page = $this->params()->fromQuery('page', 0);
        $category = $this->params()->fromQuery('category', 1);

        $data = $product->getByCategory($category, $page);
        $this->setResponse(array(
            'status' => 0,
            'data' => $data,
            'msg' => 'SUCCESS'
        ));
        return $this->response;
    }

    public function saveProductAction() {
        if (!$this->isLoggedIn()) {
            $this->setResponse(array(
                'staus' => 1,
                'msg' => 'NOT_LOG_IN'
            ));
            return $this->response;
        }
        $shop = $this->getTable('Product\Model\ShopTable');
        $shop_data = $shop->getByUserId($this->user->user_id);
        if (!$shop_data) {
            $this->setResponse(array(
                'staus' => 1,
                'msg' => 'NO_SHOP'
            ));
            return $this->response;
        }
        $request = $this->getRequest();
        $form = new EditProductForm();
        if ($request->isPost()) {
            $post_data = $request->getPost();
            $product = $this->getTable('Product\Model\ProductTable');
            $product_id = intval($post_data['product_id']);

            if ($product_id !== 0 && !$shop->checkShopProduct($shop_data->titmu_shop_id, $product_id)) {
                $this->setResponse(array(
                    'staus' => 1,
                    'msg' => 'NO_PRODUCT_IN_SHOP'
                ));
                return $this->response;
            }
            $form->setData($post_data);
            if (!$form->isValid()) {
                $this->setResponse(array(
                    'staus' => 1,
                    'msg' => 'ERROR'
                ));
                return $this->response;
            }

            /* save uploaded image */
            $image_upload = $this->getTable('Home\Model\ImageUpload');
            $images = $post_data['product_images'];

            if ($images['defaultImage']) {
                $default_image = $images['defaultImage']['url'];
            }
            if ($images['list']) {
                foreach ($images['list'] as $image) {
                    $img_array = array(
                        'image_link' => $image['url'],
                        'image_thumbnail' => $image['thumbnailUrl'],
                        'user_id' => $this->user->user_id
                    );
                    if ($row = $image_upload->checkUserLink($img_array)) {
                        $tmp_id = $row->image_uploaded_id;
                        $image_upload->doUpdate($img_array, $row->image_uploaded_id);
                    } else {
                        $image_upload->addNew($img_array);
                        $tmp_id = $image_upload->getLastInsertId();
                    }
                    if ($image['url'] == $default_image) {
                        $default_id = $tmp_id;
                    }
                }
            }

            $date = new \DateTime();
            $array = array(
                'product_name' => $post_data['product_name'],
                'product_price' => $post_data['product_price'],
                'product_desc' => $post_data['product_desc'],
                'product_owner' => $this->user->user_id,
                'titmu_shop_id' => $shop_data->titmu_shop_id,
                'date_created' => $date->format('Y:m:d h:i:s'),
                'featured_image' => $default_id
            );
            if ($product_id === 0) {
                $product->addNew($array);
            } else {
                $product->update($array, $product_id);
            }
        }
        $this->setResponse(array(
            'status' => 0,
            'msg' => 'SUCCESS'
        ));
        return $this->response;
    }

}
