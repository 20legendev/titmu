<?php

namespace Redis\Service\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Cache\Storage\Adapter\RedisOptions;
use Zend\Cache\Storage\Adapter\Redis;

class RedisCacheFactory implements FactoryInterface {
		
	public function createService(ServiceLocatorInterface $serviceLocator) {
        $config = $serviceLocator->get('config');
		$config = $config['redis-srv'];
		
        $redis_options = new RedisOptions();
        $redis_options -> setServer(array(
            'host' => $config["host"],
            'port' => $config["port"],
            'database'=>$config["database"] | 0,
            'timeout' => '30'
        ));
		$redis_cache = new Redis($redis_options);
		return $redis_cache;
	}
}

?>