<?php
namespace Redis\Model;

use Zend\ServiceManager\ServiceLocatorInterface;

class Test {
	
	public function __construct(ServiceLocatorInterface $sm) {
		$this->service = $sm;
	}

	public function getItem($key) {
		$test = $this->service->get('redis');
		return $test -> getItem($key);
	}

}
?>