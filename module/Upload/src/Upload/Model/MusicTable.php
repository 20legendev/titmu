<?php
namespace Home\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class MusicTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}
	
	public function getByCategory($cate_id, $page){
		$table = $this->tableGateway->getTable();
		$cate_id = (int)$cate_id;
		$select = new Select();
		
		$select->from(array('m'=>$table));
		$select->join(array('a'=>'tb_artist'), 'a.id = m.artist_id', array('artist_name'=>'name'));
		$select->join(array('c'=>'tb_cate'), 'c.id = m.cate_id', array('cate_name'=>'name'));
		
		$select->where(array('m.cate_id'=>$cate_id)); 
		$select->order('lastUpdate DESC');
		$select->limit(25);
		$select->offset($page ? $page: 0);
		
		$data = $this->tableGateway->selectWith($select);
		return $data;
	}
	
	public function getBySlug($slug){
		$rowset = $this -> tableGateway -> select(array('slug' => $slug));
		$row = $rowset -> current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function fetchAll() {
		$resultSet = $this -> tableGateway -> select();
		return $resultSet;
	}

	public function getAlbum($id) {
		$id = (int)$id;
		$rowset = $this -> tableGateway -> select(array('id' => $id));
		$row = $rowset -> current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveAlbum(Album $album) {
		$data = array('artist' => $album -> artist, 'title' => $album -> title, );

		$id = (int)$album -> id;
		if ($id == 0) {
			$this -> tableGateway -> insert($data);
		} else {
			if ($this -> getAlbum($id)) {
				$this -> tableGateway -> update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteAlbum($id) {
		$this -> tableGateway -> delete(array('id' => $id));
	}

}
