<?php

namespace Upload\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Home\Controller\BaseController;
use Upload\Source\UploadHandler;

class IndexController extends BaseController {
	
    public function imageAction() {
    	$upload = new UploadHandler(array(
			'image_versions'=>array(
				'' => array(
                    'max_width' => 800,
                    'max_height' => 600
                ),
				'thumbnail' => array(
                    'max_width' => 250,
                    'max_height' => 500
                )
			)
		));
		return $this->response;
    }
}
