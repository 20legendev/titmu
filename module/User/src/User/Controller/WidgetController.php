<?php
namespace User\Controller;

use Home\Controller\BaseController;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class WidgetController extends BaseController {

	public function indexAction() {
		$view = new ViewModel();
		return $view;
	}
	
	public function profileAction(){
		$view = new ViewModel();
		$user_data = $this->params()->fromRoute('user_data');
		$view->user_data = $user_data;
		return $view;
	}

}
