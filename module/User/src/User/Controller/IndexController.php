<?php

namespace User\Controller;

use DVGroup\Operation\BaseController;
use Zend\View\Model\ViewModel;

class IndexController extends BaseController {

    public function indexAction() {
        $view = new ViewModel();
        return $view;
    }

    public function registerAction() {
        $this->initFacebook();
        $access_token = $this->params()->fromPost('access_token');
        $success = false;
        $this->facebook->setAccessToken($access_token);
        $data = $this->facebook->api('/me', 'GET');
        if (!isset($data) || !$data['id']) {
            $this->setResponse([
                'status' => 1,
                'msg' => 'LOGIN_ERROR'
            ]);
            return $this->response;
        }
        $user = $this->getTable('UserTable');
        $update_arr = array(
            'fullname' => $data['name'],
            'email' => $data['email'],
            'username' => $data['username'],
            'gender' => $data['gender'] == 'male' ? 1 : 0,
            'json_data' => json_encode($data)
        );
        $userObj = $user->checkUser($data['email']);
        $auth = new \DVGroup\Users\AuthUser();
        if ($userObj) {
            $user->user_id = $userObj->user_id;
            $user->updateUser($update_arr);
            $auth->setLoggedIn($userObj->user_id);
            $success = true;
        } else if ($user->addNew($update_arr)) {
            $success = true;
            $auth->setLoggedIn($user->getLastInsertId());
        }
        if ($success) {
            $this->setResponse([
                'status' => 0,
                'msg' => 'SUCCESS'
            ]);
            return $this->response;
        }
        $this->setResponse([
            'status' => 1,
            'msg' => 'ERROR'
        ]);
        return $this->response;
    }

    private function initFacebook() {

        $sm = $this->getServiceLocator();
        $config = $sm->get('config');
        $fb_config = [
            'appId' => $config['facebook']['app_id'],
            'secret' => $config['facebook']['app_secret'],
            'fileUpload' => true,
            'allowSignedRequest' => false,
            ];
        $this->facebook = new \Facebook($fb_config);
    }

}
