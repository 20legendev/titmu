<?php

namespace User\Controller;

use User\Model\ProductLike;
use User\Model\UserCollection;
use DVGroup\Operation\BaseController;
use Zend\View\Model\ViewModel;

class AccountController extends BaseController {
	public function indexAction() {
		echo 'index';
		return $this->response;
	}
	public function homeAction() {
		echo 'home';
		return $this->response;
	}
	public function favouriteAction() {
		$params = $this->params ();
		$username = $params->fromRoute ( 'username' );
		
		$view = new ViewModel();
		$user_data = $this->prepare ( $view, $username );
		
		$collection = $this->getTable ( 'User\Model\UserCollection' );
		$product_like = $this->getTable ( 'User\Model\ProductLike' );
		$collection_data = $collection->getByUser ( $user_data->user_id );
		
		$arr = array ();
		foreach ( $collection_data as $collection_item ) {
			$arr [$collection_item->user_collection_id] = $product_like->getFourLastByCollection ( $collection_item->user_collection_id );
		}
		$products = $product_like->getByUser ( $this->user->user_id, 0 );
		
		$view->collection = $collection_data;
		$view->data = $arr;
		$view->product = \Zend\Stdlib\ArrayUtils::iteratorToArray ( $products );
		return $view;
	}
	private function prepare(&$view, $username) {
		$user = $this->getTable ( 'UserTable' );
		$user_data = $user->getByUsername ( $username );
		if (! $user_data) {
			return $this->redirect ()->toRoute ( 'user-home' );
		}
		$profile = $this->forward ()->dispatch ( 'User\Controller\Widget', array (
				'action' => 'profile',
				'user_data' => $user_data
		) );
		$view->addChild($profile, 'profile');
		return $user_data;
	}
}
