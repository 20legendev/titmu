<?php

namespace Home\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserWidget extends AbstractHelper {
		
	protected $service;
	protected $items;

	public function __construct(ServiceLocatorInterface $service) {
		$this -> service = $service;
	}

	protected function getService() {
		return $this -> service;
	}

	public function __invoke() {
		return $this;
	}

	public function getHot() {
		if ($items = $this -> __getHot())
			return $this -> getView() -> render('home/widgets/hot_subjects', array('items' => $items));
	}

	private function __getHot() {
		if (!$this -> items)
			$this -> items = $this -> getService() -> get('Home\Model\Subject') -> getHot();
		return $this -> items;
	}

}
?>