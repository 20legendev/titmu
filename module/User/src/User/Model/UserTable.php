<?php

namespace User\Model;

use DVGroup\Db\Model\BaseTable;

class UserTable extends BaseTable {

    
    public function getById($user_id) {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('email', 'username', 'fullname', 'user_id'));
        $select->where(array('user_id' => $user_id));
        $result = $this->tableGateway->selectWith($select);
        $row = $result->current();
        if ($row) {
            return get_object_vars($row);
        }
        return NULL;
    }
    
    public function checkUser($email) {
        $result = $this->tableGateway->select(array('email' => $email));
        if ($result->count() > 0) {
            return $result->current();
        }
        return null;
    }

    public function updateUser($update) {
        return $this->tableGateway->update($update, array('user_id' => $this->user_id));
    }

    public function addNew($arr) {
        return $this->tableGateway->insert($arr);
    }

    public function getByUsername($username) {
        $result = $this->tableGateway->select(array('username' => $username));
        if ($result->count() > 0) {
            return $result->current();
        }
        return null;
    }

    public function getLastInsertId() {
        return $this->tableGateway->lastInsertValue;
    }

    
    public function load() {
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('email', 'username', 'fullname', 'user_id'));
        $select->where(array('user_id' => $this->user_id));
        $result = $this->tableGateway->selectWith($select);
        if ($result->current()) {
            return $result->current();
        }
        return NULL;
    }

    public function checkShopName($shop_url) {
        $result = $this->tableGateway->select(array('shop_url' => $shop_url));
        if ($result->count() > 0) {
            return true;
        }
        return false;
    }

    public function updateShopUrl($user_id, $shop_url) {
        return $this->tableGateway->update(array('shop_url' => $shop_url), array('user_id' => $user_id));
    }

}
