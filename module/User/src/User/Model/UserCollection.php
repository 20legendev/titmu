<?php
namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use User\Model\ProductLike;
use DVGroup\Db\Model\BaseTable;

class UserCollection extends BaseTable{
		
	public function addNew($array){
		return $this->tableGateway->insert($array);
	}
	
	public function delete($id){
		return $this->tableGateway->delete(array(
			'user_collection_id'=>$id
		));
	}
	
	public function getByUser($user_id){
		$select = new Select();
		$select->from($this->tableGateway->getTable());
		$select->where(array('user_id'=>$user_id));
		$select->order('collection_name ASC');
		$result = $this->tableGateway->selectWith($select);
		$result->buffer();
		return $result;
	}
	
	public function haveDefault($user_id){
		$result = $this->tableGateway->select(array('user_id'=>$user_id, 'is_default'=>1));
		if($result->current()){
			return true;
		}
		return false;
	}
	
	public function insertDefault($arr, $user_id){
		for($i = 0; $i < count($arr); $i++){
			$this->tableGateway->insert(array(
				'collection_name'=>$arr[$i],
				'user_id'=>$user_id,
				'is_default'=>$i == 0 ? 1: 0
			));
		}
	}
	
	public function getDefault($user_id){
		$result = $this->tableGateway->select(array('user_id'=>$user_id, 'is_default'=>1));
		$row = $result->current();
		if($row){
			return $row;
		}
		return false;
	}

}
