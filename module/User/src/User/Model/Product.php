<?php
namespace Product\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Product implements InputFilterAwareInterface {
	public $product_id;
	public $product_name;
	public $product_owner;
	public $featured_image;

	public function exchangeArray($data) {
		foreach ($data as $key => $value) {
			$this -> $key = (isset($data[$key])) ? $value : null;
		}
	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {
		if (!$this -> inputFilter) {
			$inputFilter = new InputFilter();
			$factory = new InputFactory();
			$this -> inputFilter = $inputFilter;
		}
		return $this -> inputFilter;
	}

}
