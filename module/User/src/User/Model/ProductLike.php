<?php
namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ProductLike implements InputFilterAwareInterface{
		
	public $product_user_like_id;
	public $user_id;
	public $product_id;
	public $since;
    
	public function __construct(TableGateway $tableGateway){
		$this -> tableGateway = $tableGateway;
	}
	
	public function checkLike($user_id, $product_id){
		$result = $this->tableGateway->select(array('product_id'=>$product_id, 'user_id'=>$user_id));
		$row = $result->current();
		if($row) return $row;
		return; 
	}
	
	public function addNew($array){
		return $this->tableGateway->insert($array);
	}
	
	public function delete($user_id, $product_id){
		return $this->tableGateway->delete(array(
			'user_id'=>$user_id,
			'product_id'=>$product_id
		));
	}
	
	public function getByCollection($collection_id){
		$select = new Select();
		$select->from(array('pl'=>$this->tableGateway->getTable()));
		$select->join(array('p'=>'product'), 'pl.product_id = p.product_id');
		$select->join(array('i'=>'image_uploaded'), 'p.featured_image = i.image_uploaded_id');
		$select->where(array('pl.user_collection_id'=>$collection_id));
		
		$select->order('pl.since DESC');
		
		$result = $this->tableGateway->selectWith($select);
		$result->buffer();
		return $result;
	}
	
	public function getFourLastByCollection($collection_id){
		$select = new Select();
		$select->from(array('pl'=>$this->tableGateway->getTable()));
		$select->join(array('p'=>'product'), 'pl.product_id = p.product_id');
		$select->join(array('i'=>'image_uploaded'), 'p.featured_image = i.image_uploaded_id');
		$select->where(array('pl.user_collection_id'=>$collection_id));
		
		$select->order('pl.since DESC');
		$select->limit(4);
		
		$result = $this->tableGateway->selectWith($select);
		$result->buffer();
		return $result;
	}
	
	public function getByUser($user_id, $page){
		// $select = $this->tableGateway->getSql()->select();
		$select = new Select();
		$select->from(array('pl'=>$this->tableGateway->getTable()));
		$select->join(array('p'=>'product'), 'p.product_id = pl.product_id');
		$select->join(array('i'=>'image_uploaded'), 'p.featured_image = i.image_uploaded_id');
		$select->join(array('s'=>'titmu_shop'), 'p.titmu_shop_id = s.titmu_shop_id');
		$select->where(array('pl.user_id'=>$user_id));
		
		$result = $this->tableGateway->selectWith($select);
		$result->buffer();
		return $result;
	}
	
	
	public function exchangeArray($data) {
		foreach($data as $key=>$value){
			$this->$key = (isset($data[$key])) ? $value : null;
		}
	}
	
	public function getLastInsertId(){
		return $this->tableGateway->lastInsertValue;
	}
	
	public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
