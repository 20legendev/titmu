<?php
namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class UserCookieTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}
	
	public function checkCookie($cookie){
		$result = $this -> tableGateway ->select(array('cookie'=>$cookie));
		$row = $result->current();
		if($row){
			return $row; 
		}
		return NULL;
	}
	
	public function updateCookie($user_id, $cookie){
		$result = $this->tableGateway->select(array('user_id'=>$user_id));
		$row = $result->current();
		if($row){
			$this->tableGateway->update(array('cookie'=>$cookie), array('user_id'=>$user_id));
		}else{
			$this->tableGateway->insert(array('cookie'=>$cookie,'user_id'=>$user_id));
		}
	}
	
}
