<?php
namespace Product\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ProductTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this -> tableGateway = $tableGateway;
	}

	public function getByCategory($category, $page) {
		$page = intval($page);
		$select = new Select();
		$select->from(array('p'=>$this->tableGateway->getTable()));
		$select->join(array('t'=>'product_tag'), 't.product_id = p.product_id');
		$select->where(array('t.category_id'=>$category));
		$select->limit(20);
		$select->offset($page * 20);
		$resultSet = $this -> tableGateway -> selectWith($select);
		return $this->toArray($resultSet);
	}

	public function getAlbum($id) {
		$id = (int)$id;
		$rowset = $this -> tableGateway -> select(array('id' => $id));
		$row = $rowset -> current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function saveAlbum(Album $album) {
		$data = array('artist' => $album -> artist, 'title' => $album -> title, );

		$id = (int)$album -> id;
		if ($id == 0) {
			$this -> tableGateway -> insert($data);
		} else {
			if ($this -> getAlbum($id)) {
				$this -> tableGateway -> update($data, array('id' => $id));
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteAlbum($id) {
		$this -> tableGateway -> delete(array('id' => $id));
	}
	
	public function toArray($data){
		return \Zend\Stdlib\ArrayUtils::iteratorToArray($data);
	}

}
