<?php

namespace User;

use User\Model\UserTable;
use User\Model\ProductLike;
use User\Model\UserCollection;
use Zend\Db\TableGateway\TableGateway;

class Module {

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'UserTable' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new UserTable(new TableGateway('user', $dbAdapter));
                },
                'User\Model\ProductLike' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new ProductLike(new TableGateway('product_user_like', $dbAdapter));
                    return $table;
                },
                'User\Model\UserCollection' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new UserCollection(new TableGateway('user_collection', $dbAdapter));
                }
            )
        );
    }

}
