<?php
return array (
		'controllers' => array (
				'invokables' => array (
						'User\Controller\Index' => 'User\Controller\IndexController',
						'User\Controller\Account' => 'User\Controller\AccountController',
						'User\Controller\Widget' => 'User\Controller\WidgetController' 
				) 
		),
		
		'router' => array (
				'routes' => array (
						'register' => array (
								'type' => 'literal',
								'options' => array (
										'route' => '/user/dang-ky',
										'constraints' => array (),
										'defaults' => array (
												'controller' => 'User\Controller\Index',
												'action' => 'register' 
										) 
								) 
						),
						
						'user-home' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/people[/]',
										'defaults' => array (
												'controller' => 'User\Controller\Account',
												'action' => 'index' 
										) 
								),
								'may_terminate' => true,
								'child_routes' => array (
										'user-page' => array (
												'type' => 'segment',
												'options' => array (
														'route' => '[:username][/]',
														'constraints' => array (
																'username' => '[.a-zA-Z0-9_-]*' 
														),
														'defaults' => array (
																'controller' => 'User\Controller\Account',
																'action' => 'home' 
														) 
												),
												'may_terminate' => true,
												'child_routes' => array (
														'favourite' => array (
																'type' => 'segment',
																'options' => array (
																		'route' => 'yeu-thich.html',
																		'defaults' => array (
																				'controller' => 'User\Controller\Account',
																				'action' => 'favourite' 
																		) 
																),
																'may_terminate' => true 
														) 
												) 
										) 
								) 
						) 
				) 
		),
		
		'view_manager' => array (
				'template_path_stack' => array (
						'user' => __DIR__ . '/../view' 
				) 
		) 
);
