<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=titmu;host=localhost',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
    'redis-srv' => [
        'srv1' => [
            'scheme' => 'tcp',
            'host' => '127.0.0.1',
            'port' => 6379,
            'database' => 0
        ]
    ],
    'facebook' => array(
        'app_id' => '1430483450529148',
        'app_secret' => '7968d65106aa4b81113a0199cc197fd0'
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'seo'=>[
        'sitename'=>'Melody.com',
        'default'=>'Melody.com - Nơi bạn thỏa chí tung hoành lắng nghe nhịp điệu từ con tim'
    ],
    'shop' => array(
        'default_avatar' => 'img/shop/default_avatar.jpg',
        'default_featured_image' => 'img/shop/shop_default.jpg',
        'default_name' => 'Tên Cửa hàng',
        'default_desc' => 'Tất cả vì khách hàng thân thương, shop luôn tận tình mang đến những sản phẩm tốt nhất',
        'default_address' => '38 Bà Triệu, Hai Bà Trưng, Hà Nội',
        'default_contact' => '0987686868'
    )
);
