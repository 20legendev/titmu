Account = {
    loginFacebook: function () {
        FB.login(function (response) {
            if (response.status === 'connected') {
                Account.sendLogin(response.authResponse.accessToken);
            }
        }, {scope: 'email,user_likes'});
    },
    sendLogin: function (access_token) {
        $.ajax({
            url: 'user/dang-ky',
            type: "POST",
            data: {
                access_token: access_token
            },
            success: function (data) {
                if (data.status == 0) {
                    window.location.href = window.location.href;
                }
            },
            failure: function () {
            }
        });
    },
    init: function () {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '1430483450529148',
                cookie: true, // enable cookies to allow the server to access the session
                xfbml: true, // parse social plugins on this page
                version: 'v2.1', // use version 2.1
                status: true
            });
        };
    }
};

$(document).ready(function () {
    Account.init();
}); 