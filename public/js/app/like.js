Like = {
	
	init: function(){
		this.config = {
			likes: {}
		};
	},

	doLike: function(obj, productId){
		this.config.likes[productId] = this.config.likes[productId] ? 0 : 1;
		if(this.config.likes[productId]){
			obj.addClass('active');
			Notify.saveFavourite("Đã lưu vào bộ sưu tập của bạn", true);
		}else{
			obj.removeClass('active');
			Notify.saveFavourite("Đã bỏ ra khỏi bộ sưu tập", false);
		}
		$.ajax({
			url: 'pws/add-like',
			type: 'POST',
			data: {
				product_id: productId,
				is_like: this.config.likes[productId]
			},
			success: function(){
			
			},
			failure: function(){
			
			}
		});
	}
};

$(document).ready(function(){
	Like.init();
});