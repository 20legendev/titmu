Notify = {
	init: function(){
		this.config = {};
	},
	
	showNotification: function(text){
		
	},
	
	saveFavourite: function(text, isShow){
		$('#notification').remove();
		clearTimeout(this.timeout);
		var data = '<div id="notification" class="notification"><div>'+text+'</div><div class="view cun-button">Xem luôn</div></div>';
		$('body').append(data);
		setTimeout(function(){
			$('#notification').addClass('show');
			if(!isShow){
				$('#notification .view').addClass('red');
			}
		});
		this.timeout = setTimeout(function(){
			$('#notification').removeClass('show');
			setTimeout(function(){
				$('#notification').remove();
			}, 500);
		}, 2000);
	}
};
$(document).ready(function(){
	Notify.init();
});