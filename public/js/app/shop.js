Shop = {

	init : function() {
		this.config = {};
		this.initUploader();
		this.initWookmark();
	},

	initWookmark : function() {
		$('#product_container .product-item').wookmark({
			align : 'left',
			autoResize : true,
			comparator : null,
			container : $('#product_container'),
			direction : 'left',
			ignoreInactiveItems : true,
			itemWidth : 210,
			fillEmptySpace : false,
			flexibleWidth : 0,
			offset : 20,
			onLayoutChanged : undefined,
			outerOffset : 0,
			possibleFilters : [],
			resizeDelay : 50,
			verticalOffset : undefined
		});
	},

	openShop : function(e) {
		e.stopPropagation();
		e.preventDefault();
		$('#openform').submit();
	},

	editInformation : function() {
		$('#shop').append($('#edit_shop').html());
	},

	initUploader : function() {
		this.uploadImage = {
			list : {},
			defaultImage : undefined
		};
		this.uploaderTag($('#fileupload'));
	},

	closeEdit : function() {
		$('#add-item').remove();
		this.uploadImage = {
			list : {},
			defaultImage : undefined
		};
	},

	save : function(publish, url) {
		var data = {
			product_name : $('#edit-name').val(),
			product_desc : $('#edit-desc').val(),
			product_price : $('#edit-price').val(),
			publish : publish,
			product_id : $('#edit-id').val(),
			product_images : this.uploadImage
		};
		$.ajax({
			url : url,
			method : "POST",
			data : data,
			success : function(e) {
				console.log(e);
			},
			failure : function(error) {
				console.log("ERROR");
			}
		});
	},

	uploaderTag : function(tag) {
		var uploadButton = $('<button/>').addClass('btn btn-primary').prop('disabled', true).text('Processing...').on('click', function() {
			var $this = $(this), data = $this.data();
			$this.off('click').text('Abort').on('click', function() {
				$this.remove();
				data.abort();
			});
			data.submit().always(function() {
				$this.remove();
			});
		});
		console.log("A");
		tag.fileupload({
			url : '/upload/image',
			dataType : 'json',
			autoUpload : true,
			acceptFileTypes : /(\.|\/)(gif|jpe?g|png)$/i,
			maxFileSize : 10000000, // 10 MB
			disableImageResize : true
		}).on('fileuploadadd', function(e, data) {
			$('#shop').append($('#edit-item').html());
			if (!Shop.config.initloadersmall) {
				Shop.uploaderTag($('#add-more-small'));
				Shop.config.initloadersmall = 1;
			}
		}).on('fileuploadprocessalways', function(e, data) {
		}).on('fileuploadprogressall', function(e, data) {
			$('#progress').show();
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .progress-bar').css('width', progress + '%');
		}).on('fileuploaddone', function(e, data) {
			$('#progress').hide();
			$.each(data.result.files, function(index, file) {
				Shop.uploadImage.list[file.name] = file;
				if (!Shop.uploadImage.defaultImage) {
					$('#big-image').attr('src', file.url);
					Shop.uploadImage.defaultImage = file;
				} else {
					$('#add-more-small').before('<div class="small-image" style="background-image: url(' + file.url + ')" ></div>');
				}
			});
		}).on('fileuploadfail', function(e, data) {
			$('#progress').hide();
		}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	}
};

$(document).ready(function() {
	Shop.init();
}); 