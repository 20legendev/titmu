LazyLoad = {
    init: function(){
        var obj = $('.lazy-image');
        var src = '', delay=0;
        for(var i=0; i < obj.length; i++){
            src = $(obj[i]).attr('lazy-src');
            delay = parseInt($(obj[i]).attr('lazy-delay'));
            (function(jObject, imageSrc, delay){
                setTimeout(function(){
                    var img = new Image();
                    img.onload = function(){
                        jObject.css('background-image', 'url('+imageSrc+')');
                    }
                    img.src = imageSrc;
                }, delay);
            })($(obj[i]), src, delay)
        }
    }
}
SpecialSlider = {
		init: function(totalPage){
			this.wrapper = $("#speciallist-wrapper");
			this.config = {
				totalPage: totalPage,
				cur: 0,
				width: $('#speciallist-wrapper .slider').width() + 18
			}
		},
		
		next: function(){
			this.config.cur = this.config.cur < this.config.totalPage - 1 ? this.config.cur + 1 : 0;
			var translate = 'translateX(-'+(this.config.cur * this.config.width)+'px)';
			this.wrapper.css('-webkit-transform', translate);
			this.wrapper.css('-ms-transform', translate);
			this.wrapper.css('-moz-transform', translate);
			this.wrapper.css('-o-transform', translate);
			this.wrapper.css('transform', translate);
			$('#specialPaging').html((this.config.cur + 1) + '/' + this.config.totalPage);
		},
		
		prev: function(){
			this.config.cur = this.config.cur > 0 ? this.config.cur - 1 : this.config.totalPage - 1;
			var translate = 'translateX(-'+(this.config.cur * this.config.width)+'px)';
			this.wrapper.css('-webkit-transform', translate);
			this.wrapper.css('-ms-transform', translate);
			this.wrapper.css('-moz-transform', translate);
			this.wrapper.css('-o-transform', translate);
			this.wrapper.css('transform', translate);
			$('#specialPaging').html((this.config.cur + 1) + '/' + this.config.totalPage);
		}
	}
$(document).ready(function(){
	LazyLoad.init();
})